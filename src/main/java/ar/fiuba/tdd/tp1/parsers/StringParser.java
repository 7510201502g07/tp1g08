package ar.fiuba.tdd.tp1.parsers;

import ar.fiuba.tdd.tp1.EvaluablesFactory;
import ar.fiuba.tdd.tp1.IEvaluable;

public class StringParser implements IParser {
    @SuppressWarnings("CPD-START")
    private static StringParser ourInstance = new StringParser();

    private StringParser() {
    }

    public static StringParser getInstance() {
        return ourInstance;
    }

    @SuppressWarnings("CPD-END")
    @Override
    public IEvaluable parse(String param) {
        return EvaluablesFactory.getString(param);
    }


}
