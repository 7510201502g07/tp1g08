package ar.fiuba.tdd.tp1.parsers;

import ar.fiuba.tdd.tp1.EvaluablesFactory;
import ar.fiuba.tdd.tp1.IEvaluable;

public class MoneyParser implements IParser {
    private static MoneyParser ourInstance = new MoneyParser();

    private MoneyParser() {
    }

    public static MoneyParser getInstance() {
        return ourInstance;
    }


    @Override
    public IEvaluable parse(String param) {

        String symbol = "" + param.charAt(0);
        param = param.substring(1); //Remove Symbol
        param = param.replace(',', '.'); //Replace coma for dot
        double wrappingValue = Double.parseDouble(param);

        return EvaluablesFactory.getMoney(symbol, wrappingValue);
    }


}
