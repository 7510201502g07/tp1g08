package ar.fiuba.tdd.tp1.parsers;

import ar.fiuba.tdd.tp1.IEvaluable;


public interface IParser {
    IEvaluable parse(String param);
}
