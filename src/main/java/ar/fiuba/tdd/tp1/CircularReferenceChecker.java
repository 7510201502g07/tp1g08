package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.exceptions.BadReferenceException;

import java.util.ArrayList;

public class CircularReferenceChecker {

    private static final CircularReferenceChecker instance = new CircularReferenceChecker();
    private static final ArrayList<String> visitedCells = new ArrayList<>();

    public static CircularReferenceChecker getInstance() {
        return instance;
    }

    public void check(String cell) {
        if (visitedCells.contains(cell)) {
            throw new BadReferenceException();
        }
    }

    public void add(String cell) {
        visitedCells.add(cell);
    }

    public void clear() {
        visitedCells.clear();
    }

    public void removeLast() {
        visitedCells.remove(visitedCells.size() - 1);
    }
}
