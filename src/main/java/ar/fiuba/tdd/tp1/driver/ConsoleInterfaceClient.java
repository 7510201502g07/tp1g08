package ar.fiuba.tdd.tp1.driver;

import ar.fiuba.tdd.tp1.Cell;
import ar.fiuba.tdd.tp1.exceptions.BadFormulaException;
import ar.fiuba.tdd.tp1.exceptions.BadReferenceException;
import ar.fiuba.tdd.tp1.exceptions.UndeclaredWorkSheetException;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Scanner;

public class ConsoleInterfaceClient {

    static Scanner reader;
    static SpreadSheetInterfaceAdapter spreadsheet;

    static String bookName;
    static String sheetName;
    static String cellId;
    static String value;
    static String rangeName;
    static String range;

    static int command = 9;

    public static void main(String[] args) {
        reader = new Scanner(System.in, "UTF-8");
        spreadsheet = new SpreadSheetInterfaceAdapter();
        System.out.println("Welcome to SpreadSheet");
        System.out.print("\nEnter a name for the book: ");
        bookName = reader.nextLine();
        spreadsheet.createNewWorkBookNamed(bookName);
        while (command != 0) {
            showMenu();
            try {
                command = Integer.parseInt(reader.nextLine());
                chooseAndExecuteOption();
            } catch (NumberFormatException e) {
                System.out.println("Error: please enter only numbers to select an option");
            }
        }
        printProgram();
        System.out.println("\nGoodbye!");
    }

    private static void chooseAndExecuteOption() {
        if (command < 5) {
            firstHalf();
        } else {
            secondHalf();
        }
    }

    /*
     * To reduce NCSS complexity
     */
    private static void firstHalf() {
        switch (command) {
            case 1:
                createNewWorkBook();
                break;
            case 2:
                createNewWorkSheet();
                break;
            case 3:
                setCellValue();
                break;
            case 4:
                getCellValueAsDouble();
                break;
            default:
                break;
        }
    }

    /*
     * To reduce NCSS complexity
     */
    private static void secondHalf() {
        switch (command) {
            case 5:
                getCellValueAsString();
                break;
            case 6:
                addNamedRange();
                break;
            case 7:
                spreadsheet.undo();
                break;
            case 8:
                spreadsheet.redo();
                break;
            default:
                System.out.println("Invalid option. Please enter one from the list above");
                break;
        }
    }

    
    private static void addNamedRange() {
        try {
            bookName = getBookName();
            System.out.print("Enter a name for the range: ");
            rangeName = reader.nextLine();
            System.out.print("Enter the range: ");
            range = reader.nextLine();
            spreadsheet.addNamedRangeToWorkbook(bookName, rangeName, range);
        } catch (NullPointerException e) {
            System.out.println("Error: Invalid book name");
            showBookNamesInProgram();
        }
    }

    private static void printProgram() {
        for (String bookName : spreadsheet.workBooksNames()) {
            System.out.println("Book: " + bookName);
            for (String sheetName : spreadsheet.workSheetNamesFor(bookName)) {
                System.out.println(" - Sheet: " + sheetName);
                HashMap<String, Cell> cells = spreadsheet.workCellsFor(bookName, sheetName);
                for (Entry<String, Cell> cell : cells.entrySet()) {
                    try {
                        System.out.println("   - Cell " + cell.getKey() + ": " + cell.getValue().asDouble());
                    } catch (IllegalArgumentException e) {
                        System.out.println("   - Cell " + cell.getKey() + ": "
                                + spreadsheet.getCellValueAsString(bookName, sheetName, cell.getKey()));
                    }
                }
            }
        }
    }

    @SuppressWarnings("CPD-START")
    private static void createNewWorkSheet() {
        try {
            bookName = getBookName();
            System.out.print("Enter the new sheet name: ");
            sheetName = reader.nextLine();
            spreadsheet.createNewWorkSheetNamed(bookName, sheetName);
        } catch (NullPointerException e) {
            System.out.println("Error: Invalid book name");
            showBookNamesInProgram();
        } catch (IllegalArgumentException e) {
            System.out.println("Error: Sheet already exists. Please enter a new name.");
        }
    }
    
    private static void createNewWorkBook() {
        System.out.print("Enter the new book name: ");
        bookName = reader.nextLine();
        spreadsheet.createNewWorkBookNamed(bookName);
    }

    private static void setCellValue() {
        try {
            askParameters();
            System.out.print("Enter a value for the cell: ");
            value = reader.nextLine();
            spreadsheet.setCellValue(bookName, sheetName, cellId, value);
        } catch (NullPointerException e) {
            System.out.println("Error: Invalid book name");
            showBookNamesInProgram();
        } catch (UndeclaredWorkSheetException e) {
            System.out.println("Error: Invalid sheet name");
            showSheetNamesInProgram();
        }
    }

    public static void getCellValueAsDouble() {
        try {
            askParameters();
            System.out.println(spreadsheet.getCellValueAsDouble(bookName, sheetName, cellId));
        } catch (NullPointerException e) {
            System.out.println("Error: Invalid book name");
            showBookNamesInProgram();
        } catch (UndeclaredWorkSheetException e) {
            System.out.println("Error: Invalid sheet name");
            showSheetNamesInProgram();
        } catch (BadFormulaException e) {
            System.out.println("Error: Invalid cell id or value is not double");
        } catch (BadReferenceException e) {
            System.out.println("Error: some cells of the range doesn't exists");
        }
    }
    
    @SuppressWarnings("CPD-END")
    private static void getCellValueAsString() {
        try {
            askParameters();
            System.out.println(spreadsheet.getCellValueAsString(bookName, sheetName, cellId));
        } catch (NullPointerException e) {
            System.out.println("Invalid book name");
            showBookNamesInProgram();
        } catch (UndeclaredWorkSheetException e) {
            System.out.println("Error: Invalid sheet name");
            showSheetNamesInProgram();
        } catch (BadFormulaException e) {
            System.out.println("Error: Invalid cell id or value is not string");
        } catch (BadReferenceException e) {
            System.out.println("Error: some cells of the range doesn't exists");
        }
    }

    private static void askParameters() {
        bookName = getBookName();
        sheetName = getSheetName();
        System.out.print("Enter a cell id: ");
        cellId = reader.nextLine();
    }

    private static void showMenu() {
        System.out.println("\nChoose the option to execute.\n"
                + "1- Create new Book\n"
                + "2- Create new Sheet\n"
                + "3- Set value to Cell\n"
                + "4- Get value as double from Cell\n"
                + "5- Get value as string from Cell\n"
                + "6- Add named range\n"
                + "7- Undo\n"
                + "8- Redo\n"
                + "0- Print program and Exit\n");
    }
    
    private static String getSheetName() {
        List<String> sheets = spreadsheet.workSheetNamesFor(bookName);
        if (sheets.size() > 1) {
            System.out.print("Enter the name of the sheet: ");
            sheetName = reader.nextLine();
        } else {
            sheetName = sheets.get(0);
        }
        return sheetName;
    }

    private static String getBookName() {
        List<String> books = spreadsheet.workBooksNames();
        if (books.size() > 1) {
            System.out.print("Enter the name of the book: ");
            bookName = reader.nextLine();
        } else {
            bookName = books.get(0);
        }
        return bookName;
    }
    
    private static void showBookNamesInProgram() {
        System.out.println("Please enter a name in the list or create a new book");
        for (String bookName : spreadsheet.workBooksNames()) {
            System.out.println(bookName);
        }
    }
    
    private static void showSheetNamesInProgram() {
        System.out.println("Please enter a name in the list or create a new sheet");
        for (String sheetName : spreadsheet.workSheetNamesFor(bookName)) {
            System.out.println(sheetName);
        }
    }
    
}
