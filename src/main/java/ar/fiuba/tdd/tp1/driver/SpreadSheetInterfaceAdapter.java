package ar.fiuba.tdd.tp1.driver;

import ar.fiuba.tdd.tp1.*;
import ar.fiuba.tdd.tp1.exceptions.BadFormatException;
import ar.fiuba.tdd.tp1.exceptions.BadFormulaException;
import ar.fiuba.tdd.tp1.formatters.DoubleFormatter;
import ar.fiuba.tdd.tp1.formatters.FormatterFactory;
import ar.fiuba.tdd.tp1.iteracion3era.NamedRanges;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SpreadSheetInterfaceAdapter implements SpreadSheetTestDriver {

    public static final String CELL_REGEXP = "([A-Z]{1,3})([0-9]{1,5})";
    public static final String RANGE_FORMULA_REGEXP1 = "[A-Z]{3,7}\\(([A-Z]{1,3}[0-9]{1,5}):([A-Z]{1,3}[0-9]{1,5})\\)";
    public static final String RANGE_FORMULA_REGEXP2 = "[A-Z]{3,7}\\(([A-Z]{1,3}[0-9]{1,5})(,[A-Z]{1,3}[0-9]{1,5})*\\)";
    
    private HashMap<String, Book> workBooksMap;
    private ArrayList<String> parameters;
    private Stack<String> undoStack;
    private Stack<String> redoStack;
    private HashMap<String, DoubleFormatter> formatters;
    
    private NamedRanges namedRanges;

    public SpreadSheetInterfaceAdapter() {
        workBooksMap = new HashMap<>();
        undoStack = new Stack<>();
        redoStack = new Stack<>();
        parameters = new ArrayList<>();
        formatters = new HashMap<>();
        addBaseFormatters();
        namedRanges = new NamedRanges();
    }

    private void addBaseFormatters() {
        formatters.put("Currency", FormatterFactory.getMoneyFormatter("$", false));
        formatters.put("Date", FormatterFactory.getDateDMYFormatter());
        formatters.put("String", FormatterFactory.getStringFormatter());
    }

    public List<String> workBooksNames() {
        return new ArrayList<>(workBooksMap.keySet());
    }

    public void createNewWorkBookNamed(String name) {
        workBooksMap.put(name, new Book(name));
        createNewWorkSheetNamed(name, "default");
    }

    public void createNewWorkSheetNamed(String workbookName, String name) {
        parameters = new ArrayList<>();
        parameters.add(name);
        workBooksMap.get(workbookName).createSheet(parameters);
        undoStack.push(workbookName);
        redoStack.clear();
    }

    public List<String> workSheetNamesFor(String workBookName) {
        return workBooksMap.get(workBookName).getSheetsNames();
    }
    
    public HashMap<String, Cell> workCellsFor(String workBookName, String workSheetName) {
        return workBooksMap.get(workBookName).getSheet(workSheetName).getCellMap();
    }
    
    public void setCellValue(String workBookName, String workSheetName, String cellId, String value) {
        value = namedRanges.replaceNamedRangeIfItHas(value);
        
        value = replaceSpacesIfItPrintfOperation(value);
        
        String command = formatCell(workSheetName, cellId) + " " + value;
        int delimiter = 0;
        if (value.substring(value.length() - 1).equals(" ")) {
            delimiter = -1;
        }
        parameters = new ArrayList<>(Arrays.asList(command.split(" ", delimiter)));
        if (parameters.contains("=")) {
            parameters = addSheetNames(workSheetName, parameters);
        }
        
        workBooksMap.get(workBookName).assignValue(parameters);
        undoStack.push(workBookName);
        redoStack.clear();
    }

    private String formatCell(String workSheetName, String cellId) {
        return "!" + workSheetName + "." + cellId;
    }

    private ArrayList<String> addSheetNames(String workSheetName, ArrayList<String> parameters) {
        
        if ( parameters.get(2).startsWith("PRINTF")) {
            String aux = parameters.get(2);
            parameters.set(2, addShetNamesToPrintfValue(workSheetName, aux));
            return parameters;
        }
        
        ArrayList<String> aux = new ArrayList<>();
        for (String s : parameters) {
            if (isValidCell(s)) {
                aux.add(formatCell(workSheetName, s));
            } else {
                if (matchesRegexp(s, RANGE_FORMULA_REGEXP1)) {
                    aux.add(addSheetNamesRangeFormula1(workSheetName, s));
                } else {
                    if (matchesRegexp(s, RANGE_FORMULA_REGEXP2)) {
                        aux.add(addSheetNamesRangeFormula2(workSheetName, s));
                    } else {
                        aux.add(s);
                    }
                }
            }
        }
        return aux;
    }

    private boolean matchesRegexp(String expression, String regexp) {
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(expression);

        return matcher.matches();
    }

    private String addSheetNamesRangeFormula1(String workSheetName, String expression) {
        expression = expression.replace("(", "(" + "!" + workSheetName + ".");
        return expression.replace(":", ":" + "!" + workSheetName + ".");
    }

    private String addSheetNamesRangeFormula2(String workSheetName, String expression) {
        ArrayList<String> tokens;
        tokens = new ArrayList<>(Arrays.asList(expression.split(",")));

        String result = tokens.remove(0);

        result = result.replace("(", "(" + "!" + workSheetName + ".");

        StringBuffer buf = new StringBuffer();
        buf.append(result);
        for (String s : tokens) {
            buf.append(",");
            buf.append(formatCell(workSheetName, s));
        }
        return buf.toString();
    }

    private boolean isValidCell(String cellId) {
        Pattern pattern = Pattern.compile(CELL_REGEXP);
        Matcher matcher = pattern.matcher(cellId);
        return matcher.matches();
    }

    public String getCellValueAsString(String workBookName, String workSheetName, String cellId) {
        String command = formatCell(workSheetName, cellId);
        
        parameters = new ArrayList<>(Arrays.asList(command.split(" ")));
        
        return workBooksMap.get(workBookName).getValue(parameters);
    }

    public double getCellValueAsDouble(String workBookName, String workSheetName, String cellId) {
        String command = formatCell(workSheetName, cellId);
        parameters = new ArrayList<>(Arrays.asList(command.split(" ")));
        try {
            return Double.parseDouble(workBooksMap.get(workBookName).getValue(parameters));
        } catch (IllegalArgumentException e) {
            throw new BadFormulaException();
        } catch (ar.fiuba.tdd.tp1.formatters.BadFormatException e) {
            throw new BadFormatException();
        }
    }

    public void undo() {
        if (undoStack.isEmpty()) {
            throw new IllegalArgumentException();
        }
        String workBookName = undoStack.pop();
        workBooksMap.get(workBookName).undo();
        redoStack.push(workBookName);
    }

    public void redo() {
        if (redoStack.isEmpty()) {
            return;
        }
        String workBookName = redoStack.pop();
        workBooksMap.get(workBookName).redo();
        undoStack.push(workBookName);
    }

    @Override
    public void setCellFormatter(String workBookName, String workSheetName, String cellId, String formatter, String format) {
        Book book = workBooksMap.get(workBookName);
        cellId = formatCell(workSheetName, cellId);
        book.setCellFormatParam(cellId, formatter, format);
    }

    @Override
    public void setCellType(String workBookName, String workSheetName, String cellId, String type) {
        Book book = workBooksMap.get(workBookName);
        cellId = formatCell(workSheetName, cellId);
        book.setCellFormat(cellId, formatters.get(type));
    }

    @Override
    public void persistWorkBook(String workBookName, String fileName) {
        Book book = workBooksMap.get(workBookName);
        JsonPersistence.persistBook(book, fileName);
    }

    @Override
    public void reloadPersistedWorkBook(String fileName) {
        Book book = JsonPersistence.loadBook(fileName);
        workBooksMap.put(book.getName(), book);
    }

    @Override
    public void saveAsCSV(String workBookName, String sheetName, String path) {
        Sheet sheet = workBooksMap.get(workBookName).getSheet(sheetName);
        CsvPersistence.writeSheet(sheet, path);
    }

    @Override
    public void loadFromCSV(String workBookName, String sheetName, String path) {
        if (workBooksMap.containsKey(workBookName)) {
            workBooksMap.remove(workBookName);
        }
        createNewWorkBookNamed(workBookName);
        createSheetIfDoNotExists(workBookName, sheetName);
        List<Pair<String, String>> cellList = CsvPersistence.readSheet(path);
        for (Pair<String, String> cell : cellList) {
            setCellValue(workBookName, sheetName, cell.first(), cell.second());
        }
    }

    @Override
    public int sheetCountFor(String workBookName) {
        return workBooksMap.get(workBookName).getSheetsNames().size();
    }

    private void createSheetIfDoNotExists(String workBookName, String sheetName) {
        if (!workBooksMap.get(workBookName).getSheetsNames().contains(sheetName)) {
            createNewWorkSheetNamed(workBookName, sheetName);
        }
    }
    
    public void addNamedRangeToWorkbook(String workBookName, String rangeName, String range) {
        this.namedRanges.addNamedRange(rangeName, range);
        this.workBooksMap.get(workBookName).addNamedRange(rangeName, range);
    }
    
    private String replaceSpacesIfItPrintfOperation(String value) {
        
        if ( value.startsWith("= PRINTF") == false) {
            return value;
        }
        
        String replacedText = value.replaceAll(" ", "\\\\\\\\b");
        replacedText = replacedText.replaceFirst("\\\\\\\\b", " ");
        
        return replacedText;
        
    }
    
    private String addShetNamesToPrintfValue(String sheetName, String parameter) {
        
        Pattern pattern = Pattern.compile("\"(.*?)\"");
        Matcher matcher = pattern.matcher(parameter);
        String original = "";
        if (matcher.find()) {
            original = matcher.group(0);
        }
        
        String replaced = parameter.replaceAll(",", ",!" + sheetName + ".");
        replaced = replaced.replace("$", "a");
        replaced = replaced.replace("\\", "a");
        
        int idxPrimerComillas =  replaced.indexOf('"');
        int idxSegundaComillas =  replaced.indexOf('"', idxPrimerComillas + 1);
        
        String reemplazo = replaced.substring(idxPrimerComillas, idxSegundaComillas + 1);
        
        replaced = replaced.replace(reemplazo, original);
        
        return replaced;
    }
}
