package ar.fiuba.tdd.tp1.iteracion3era;

import ar.fiuba.tdd.tp1.IEvaluable;
import ar.fiuba.tdd.tp1.operations.IRangeOperation;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class OpToday implements IRangeOperation {

    @Override
    public String operate(List<IEvaluable> cellList) {

        if (cellList.size() != 0) {
            return "ERROR: BAD FORMULA";
        }

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        return dateFormat.format(date);
    }

}
