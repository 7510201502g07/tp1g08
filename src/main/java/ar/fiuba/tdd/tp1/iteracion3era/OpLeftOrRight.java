package ar.fiuba.tdd.tp1.iteracion3era;

import ar.fiuba.tdd.tp1.IEvaluable;
import ar.fiuba.tdd.tp1.operations.IRangeOperation;

import java.util.List;

public class OpLeftOrRight implements IRangeOperation {

    private int count  = 0;
    boolean     isLeft = false;

    public OpLeftOrRight(String string) {
        if (string.equals("left")) {
            isLeft = true;
        } else if (string.equals("right")) {
            isLeft = false;
        }
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String operate(List<IEvaluable> cellList) {

        if (cellList.size() != 1) {
            return "ERROR: BAD FORMULA";
        }

        String value = cellList.get(0).getValue();
        if (isLeft) {
            return value.substring(0, Math.min(count, value.length()));
        } else {
            return value.substring(Math.max(value.length() - count, 0), value.length());
        }
    }
}
