package ar.fiuba.tdd.tp1.iteracion3era;

import ar.fiuba.tdd.tp1.IEvaluable;
import ar.fiuba.tdd.tp1.operations.IRangeOperation;

import java.util.List;

public class Printf implements IRangeOperation {

    private String text = "";

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String operate(List<IEvaluable> cellList) {

        if (this.getParameters() != cellList.size()) {
            return "ERROR: BAD FORMULA";
        }

        Integer index = 0;
        String finalText = this.text;
        for (IEvaluable cell : cellList) {
            finalText = finalText.replaceAll("\\$" + index, cell.getValue());
            index++;
        }

        return finalText;

    }

    private int getParameters() {

        int counter = 0;
        for (int i = 0; i < this.text.length(); i++) {
            if (this.text.charAt(i) == '$') {
                counter++;
            }
        }

        return counter;
    }

}
