package ar.fiuba.tdd.tp1.iteracion3era;

import java.util.HashMap;

public class NamedRanges {

    private HashMap<String, String> namesMap = null;
    private HashMap<String, String> rangesMap = null;
    
    public NamedRanges() {
        rangesMap = new HashMap<>();
        namesMap = new HashMap<>();
    }
    
    public void addNamedRange(String rangeName, String range) {
        this.namesMap.put(rangeName, range);
        this.rangesMap.put(range, rangeName);
    }
    
    public String replaceNamedRangeIfItHas(String parameter) {
        
        String replacedParameter = parameter;
        int index = parameter.indexOf('(');
        
        if ( index < 0 || index > parameter.length() ) {
            return parameter;
        }
        
        String parenthesisPart = parameter.substring( index );
        
        String replacedParenthesisPart = parenthesisPart;
        
        for ( String name : this.namesMap.keySet() ) {
            if ( parenthesisPart.contains(name) ) {
                replacedParenthesisPart = parenthesisPart.replaceAll(name, namesMap.get(name) );
                replacedParameter = parameter.replaceAll("\\(.*\\)", replacedParenthesisPart);
            }
        }
        
        return replacedParameter;
    }
    
    public String replaceRangeIfItHas(String parameter) {
        
        String replacedParameter = parameter;
        int index = parameter.indexOf('(');
        if ( index < 0 || index > parameter.length() ) {
            return parameter;
        }
        
        String parenthesisPart = parameter.substring( index );
        
        String replacedParenthesisPart = parenthesisPart;
        
        for ( String name : this.rangesMap.keySet() ) {
            if ( parenthesisPart.contains(name) ) {
                replacedParenthesisPart = parenthesisPart.replaceAll(name, rangesMap.get(name) );
                replacedParameter = parameter.replaceAll("\\(.*\\)", replacedParenthesisPart);
            }
        }
        
        return replacedParameter;
        
    }
}
