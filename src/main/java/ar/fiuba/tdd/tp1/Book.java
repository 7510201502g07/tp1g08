package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.exceptions.UndeclaredWorkSheetException;
import ar.fiuba.tdd.tp1.formatters.DoubleFormatter;
import ar.fiuba.tdd.tp1.iteracion3era.NamedRanges;

import java.util.*;

/**
 * Main class of FiubaSpreadSheet. Receives commands, evaluates and executes them.
 */
public class Book {

    private String name;
    private HashMap<String, Sheet> sheetMap;
    private HashMap<String, ISSheetCommand> commandMap;
    
    private NamedRanges namedRanges;

    /**
     * At undo, a pop() is done to undoStack so as to get the state of the las Cell modified, before it was modified.
     * The current Formula of the Cell is pushed to the redoStack, and the new Formula from the undoStack
     * is placed in the map.
     * The behavior is identical but opposite when redo is executed.
     */

    private Stack<IAction> undoStack;
    private Stack<IAction> redoStack;

    public Book() {
        sheetMap = new HashMap<>();
        commandMap = new HashMap<>();
        undoStack = new Stack<>();
        redoStack = new Stack<>();
        name = "";
        initializeCommandMap();
        namedRanges = new NamedRanges();
    }

    public Book(String name) {
        this();
        this.name = name;
    }

    public List<String> getSheetsNames() {
        return new ArrayList<>(sheetMap.keySet());
    }

    private void initializeCommandMap() {
        commandMap.put("ASSIGN", this::assignValue);
        commandMap.put("GET", this::getValue);
        commandMap.put("CREATE", this::createSheet);
    }

    /**
     * Evaluates and executes the command given.
     * GET command return the value of the Cell to get.
     * Other commands return 0.
     */
    public String evaluateCommand(String input) {
        if (input == null) {
            throw new IllegalArgumentException();
        }
        List<String> tokens = new ArrayList<>(Arrays.asList(input.split(" ")));
        String commandToken = tokens.remove(0);

        Optional<ISSheetCommand> optional;
        optional = Optional.ofNullable(commandMap.get(commandToken));

        ISSheetCommand command = optional.orElseThrow(IllegalArgumentException::new);

        return command.execute(tokens);
    }

    /**
     * Returns 0 if success, throws IllegalArgumentException if a sheet with the same id already exists.
     */
    public String createSheet(List<String> params) {
        if (params.isEmpty()) {
            throw new IllegalArgumentException();
        }
        String sheetId = params.get(0);
        if (sheetMap.containsKey(sheetId)) {
            throw new IllegalArgumentException();
        }
        sheetMap.put(sheetId, new Sheet());

        createUndoSheetCreation(sheetId);

        return "OK";
    }

    private void createUndoSheetCreation(String sheetId) {
        IAction action = new IAction() {

            @Override
            public void execute() {
                sheetMap.remove(sheetId);
            }

            public void revert() {
                sheetMap.put(sheetId, new Sheet());
            }
        };
        undoStack.add(action);

    }

    public Sheet getSheet(String sheetId) {

        Sheet sheet = sheetMap.get(sheetId);
        if (sheet == null) {
            throw new UndeclaredWorkSheetException();
        }
        return sheet;

        //Optional<Sheet> optSheet = Optional.ofNullable(sheetMap.getOrDefault(sheetId, null));
        //return optSheet.orElseThrow(UndeclaredWorkSheetException::new);
    }

    /**
     * Returns an array [sheetId, cellId].
     */
    private String[] getIds(String fullCellId) {
        if (fullCellId.charAt(0) != '!') {
            throw new IllegalArgumentException();
        }

        return fullCellId.substring(1).split("\\.");
    }

    /**
     * fullCellId's format is Hn:X, where n it's the sheet number and X the cell id.
     */
    private Cell getCell(String fullCellId) {
        String[] ids = getIds(fullCellId);
        Optional<Sheet> optSheet = Optional.ofNullable(sheetMap.getOrDefault(ids[0], null));
        return optSheet.orElseThrow(UndeclaredWorkSheetException::new).getCell(ids[1]);
    }

    /**
     * Assigns a new Formula to the indicated Cell. The cell's old value is pushed to the undoStack.
     */
    public String assignValue(List<String> params) {
        
        String[] ids = getIds(params.get(0));
        String sheetId = ids[0];
        Sheet sheet = getSheet(sheetId);
        String cellId = ids[1];
        Cell cell;
        cell = sheet.getCell(cellId);
        
        final IEvaluable oldValue = cell.value();

        Parser parser = Parser.getInstance();
        parser.subscribeParser("![a-zA-Z][a-zA-Z0-9]*.[a-zA-Z][a-zA-Z]*[0-9][0-9]*", this::getCell);
        params.remove(0); //Cell ID
        String formula = String.join(" ", params);
        
        cell.setValue(parser.parse(formula));

        createUndoCellAssign(cell, oldValue);
        redoStack.clear();
        return "OK";
        
    }

    private void createUndoCellAssign(final Cell cell, final IEvaluable oldValue) {
        IAction action = new IAction() {
            IEvaluable newValue = cell.value();

            @Override
            public void execute() {
                cell.setValue(oldValue);
            }

            public void revert() {
                cell.setValue(newValue);
            }
        };
        undoStack.add(action);
    }

    /**
     * Gets and returns the current value of the Cell indicated.
     */
    public String getValue(List<String> params) {
        CircularReferenceChecker crChecker = CircularReferenceChecker.getInstance();
        crChecker.clear();
        String fullId = params.get(0);
        Cell cell = getCell(fullId);
       
        return cell.getValue();
    }

    /**
     * Gets and returns the current value of the Cell indicated.
     */
    public double getDoubleValue(List<String> params) {
        CircularReferenceChecker crChecker = CircularReferenceChecker.getInstance();
        crChecker.clear();
        String fullId = params.get(0);
        Cell cell = getCell(fullId);

        return cell.asDouble();
    }

    /**
     * Performs undo operation.
     */
    public void undo() {
        if (undoStack.isEmpty()) {
            throw new IllegalArgumentException();
        }
        IAction action = undoStack.pop();
        action.execute();
        redoStack.push(action);
    }

    /**
     * Performs redo operation.
     */
    public void redo() {
        if (redoStack.isEmpty()) {
            throw new IllegalArgumentException();
        }
        IAction action = redoStack.pop();
        undoStack.push(action);
        action.revert();
    }

    public void setCellFormat(String cellId, DoubleFormatter formatter) {
        Cell cell = getCell(cellId);
        cell.setFormatter(formatter);
    }

    public void setCellFormatParam(String cellId, String param, String value) {
        Cell cell = getCell(cellId);
        cell.setFormatterParam(param, value);
    }

    public HashMap<String, Sheet> getSheetMap() {
        return sheetMap;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public void addNamedRange(String rangeName, String range) {
        this.namedRanges.addNamedRange(rangeName, range);
    }

    private interface ISSheetCommand {
        String execute(List<String> params);
    }

    private interface IAction {
        void execute();

        default void revert() {
        }
    }
}