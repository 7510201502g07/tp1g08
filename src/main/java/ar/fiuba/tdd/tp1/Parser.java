package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.iteracion3era.OpLeftOrRight;
import ar.fiuba.tdd.tp1.iteracion3era.OpToday;
import ar.fiuba.tdd.tp1.iteracion3era.Printf;
import ar.fiuba.tdd.tp1.operations.Average;
import ar.fiuba.tdd.tp1.operations.Concatenation;
import ar.fiuba.tdd.tp1.operations.IBinaryOperation;
import ar.fiuba.tdd.tp1.operations.IRangeOperation;
import ar.fiuba.tdd.tp1.operations.Maximum;
import ar.fiuba.tdd.tp1.operations.Minimum;
import ar.fiuba.tdd.tp1.parsers.DateParser;
import ar.fiuba.tdd.tp1.parsers.DoubleParser;
import ar.fiuba.tdd.tp1.parsers.IParser;
import ar.fiuba.tdd.tp1.parsers.MoneyParser;
import ar.fiuba.tdd.tp1.parsers.StringParser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Parser for the assign value command. Implements the Singleton Pattern.
 * Receives the expression that is being assigned as string, parses it, and
 * returns an IEvaluable that evaluates to that expression's result.
 */
public class Parser {

    public static final String                SINGLE_SPACE  = " ";
    public static final String                DOUBLE_REGEXP = "^-?\\d+([\\.,]\\d+)? {0,}";
    public static final String                MONEY_REGEXP  = "-?[^a-zA-Z0-9]?\\d+([\\.,]\\d+)?";
    public static final String                DATE_REGEXP   = "[0-9]{1,4}-[0-9]{1,2}-[0-9]{1,4}T?[0-6]{0,2}:?[0-6]{0,2}:?[0-6]{0,2}:?Z?";
    private static final Parser               instance      = new Parser();
    private HashMap<String, IBinaryOperation> binaryOperations;
    private HashMap<String, IRangeOperation>  rangeOperations;
    private HashMap<String, IParser>          parserHashMap;                                                                             // FIXME
                                                                                                                                         // I
                                                                                                                                         // don't
                                                                                                                                         // really
                                                                                                                                         // like
                                                                                                                                         // this
                                                                                                                                         // name...

    private Parser() {
        binaryOperations = new HashMap<>();
        binaryOperations.put("+", (double firstOp, double secondOp) -> firstOp + secondOp);
        binaryOperations.put("-", (double firstOp, double secondOp) -> firstOp - secondOp);
        binaryOperations.put("*", (double firstOp, double secondOp) -> firstOp * secondOp);
        binaryOperations.put("/", (double firstOp, double secondOp) -> firstOp / secondOp);
        binaryOperations.put("^", (double firstOp, double secondOp) -> Math.pow(firstOp, secondOp));

        rangeOperations = new HashMap<>();
        rangeOperations.put("MAX", new Maximum());
        rangeOperations.put("MIN", new Minimum());
        rangeOperations.put("AVERAGE", new Average());
        rangeOperations.put("CONCAT", new Concatenation());

        // inicio agregado 3era iteracion
        rangeOperations.put("PRINTF", new Printf());
        rangeOperations.put("LEFT", new OpLeftOrRight("left"));
        rangeOperations.put("RIGHT", new OpLeftOrRight("right"));
        rangeOperations.put("TODAY", new OpToday());
        // fin agregado 3era iteracion

        this.parserHashMap = new HashMap<>();
        this.subscribeParser(DOUBLE_REGEXP, DoubleParser.getInstance());
        this.subscribeParser(MONEY_REGEXP, MoneyParser.getInstance());
        this.subscribeParser(DATE_REGEXP, DateParser.getInstance());
    }

    public static Parser getInstance() {
        return instance;
    }

    /**
     * https://en.wikipedia.org/w/index.php?title=Hexavigesimal&oldid=578218059#
     * Bijective_base-26
     */
    public static String toBase26(int number) {
        StringBuilder ret = new StringBuilder();
        while (number > 0) {
            --number;
            ret.append((char) ('A' + number % 26));
            number /= 26;
        }
        // reverse the result, since its
        // digits are in the wrong order
        return ret.reverse().toString();
    }

    public void subscribeParser(String string, IParser parser) {
        parserHashMap.put(string, parser);
    }

    public IEvaluable parse(String input) {
        if (isString(input)) {
            return StringParser.getInstance().parse(input);
        }

        input = this.reformat(input);

        ArrayList<String> tokens;

        tokens = new ArrayList<>(Arrays.asList(input.split(" ")));

        IEvaluable firstOperand = getOperand(tokens);

        while (tokens.size() > 0) {
            IBinaryOperation op = binaryOperations.get(tokens.remove(0)); // Always
                                                                          // comes
                                                                          // the
                                                                          // operation
                                                                          // token
            IEvaluable secondOperand = getOperand(tokens);
            firstOperand = new Formula(firstOperand, secondOperand, op);
        }

        return firstOperand;
    }

    private boolean isRangeOp(ArrayList<String> tokens) {
        return rangeOperations.containsKey(tokens.get(0));
    }

    private IEvaluable getRangeOp(ArrayList<String> tokens) {
        int size = tokens.size();
        if (!tokens.get(1).equals("(") || !tokens.get(size - 1).equals(")")) {
            throw new IllegalArgumentException();
        }
        ifIsPrintfSetText(tokens);
        ifIsLeftOrRightSetCount(tokens);

        String value = tokens.remove(0);
        final IRangeOperation op = rangeOperations.get(value);
            
        replaceOperators(value);

        tokens.remove(0);// Remove open brace

        Range<String> range = new Range<>();
        if (size == 4) {
            range = calcRange(tokens);
        }
        tokens.remove(0); // Remove range
        if (size == 4) {
            tokens.remove(0); // Remove closing
        }
        List<IEvaluable> cells = new ArrayList<>();
        range.forEach(cellId -> cells.add(parse("= " + cellId)));
        return new RangeFormula(cells, op);
    }

    private void replaceOperators(String value) {
        if (value.equals("LEFT")) {
            rangeOperations.replace("LEFT", new OpLeftOrRight("left"));
        } else if (value.equals("RIGHT")) {
            rangeOperations.replace("RIGHT", new OpLeftOrRight("right"));
        } else if (value.equals("PRINTF")) {
            rangeOperations.replace("PRINTF", new Printf());
        } 
    }

    private Range<String> calcRange(ArrayList<String> tokens) {
        if (tokens.get(0).matches("[^:]*:[^:]*")) {
            return getRange(tokens.get(0).split(":")[0], tokens.get(0).split(":")[1]);
        } else {
            return getRange(tokens.get(0).split(","));
        }
    }

    private void ifIsLeftOrRightSetCount(ArrayList<String> tokens) {
        if (tokens.get(0).equals("LEFT") || tokens.get(0).equals("RIGHT")) {

            int idxComma = tokens.get(2).indexOf(',');

            int count = Integer.parseInt(tokens.get(2).substring(idxComma + 1, tokens.get(2).length()));
            ((OpLeftOrRight) rangeOperations.get(tokens.get(0))).setCount(count);
            tokens.set(2, tokens.get(2).substring(0, idxComma));
        }
    }

    private Range<String> getRange(String[] cells) {
        Range<String> range = new Range<>();
        for (String cell : cells) {
            range.add(cell);
        }
        return range;
    }

    private Range<String> getRange(String init, String end) {
        Range<String> range = new Range<>();
        String sheet = init.split("\\.")[0];

        init = init.split("\\.")[1];
        String initialLetter = init.split("[0-9]")[0];
        int initialNumber = Integer.parseInt(init.split("[A-Z]")[1]);

        end = end.split("\\.")[1];
        String endLetter = end.split("[0-9]")[0];
        int endNumber = Integer.parseInt(end.split("[A-Z]")[1]);

        int index = getInitialIndex(initialLetter);

        String stringIndex = toBase26(index);
        while (stringIndex.compareTo(endLetter) != 1) {
            int numberIndex = initialNumber;
            while (numberIndex <= endNumber) {
                String cellId = sheet + "." + stringIndex + String.valueOf(numberIndex);
                range.add(cellId);
                numberIndex++;
            }
            index++;
            stringIndex = toBase26(index);
        }
        return range;
    }

    private int getInitialIndex(String initialLetter) {
        int index = 0;
        while (toBase26(index).compareTo(initialLetter) == -1) {
            index++;
        }
        return index;
    }

    private boolean isString(String input) {
        if (input == null || input.isEmpty()) {
            throw new IllegalArgumentException();
        }
        if (input.charAt(0) == '=') {
            return false;
        }

        boolean isParseable = input.matches(DATE_REGEXP) || input.matches(DOUBLE_REGEXP) || input.matches(MONEY_REGEXP);
        return !isParseable;
    }

    private int findCloseBrace(int initialIndex, List<String> string) {
        int index = initialIndex;
        int openBraces = 0;
        while (index < string.size()) {
            String character = string.get(index);
            if ("(".equals(character)) {
                openBraces++;
            }
            if (")".equals(character)) {
                if (openBraces == 0) {
                    return index;
                }
                openBraces--;
            }
            index++;
        }

        throw new IllegalArgumentException("Missing close parenthesis");
    }

    private String reformat(String expression) {
        expression = expression.replace("=", "");
        expression = expression.replace("(", " ( ");
        expression = expression.replace(")", " ) "); // Separate braces from
                                                     // number
        List<String> tokens = removeWhiteSpaces(expression);

        tokens = removeRedundantBraces(tokens);

        expression = String.join(SINGLE_SPACE, tokens);
        return expression.trim(); // Just to be sure
    }

    private List<String> removeRedundantBraces(List<String> tokens) {
        if (tokens.size() < 3 || !"(".equals(tokens.get(0))) {
            return tokens;
        }

        if (findCloseBrace(1, tokens) != (tokens.size() - 1)) {
            return tokens;
        }

        tokens = tokens.subList(1, tokens.size() - 1);
        return this.removeRedundantBraces(tokens);
    }

    private ArrayList<String> removeWhiteSpaces(String expression) {
        ArrayList<String> tokens = new ArrayList<>(Arrays.asList(expression.split(SINGLE_SPACE)));
        List<String> suppressCharacter = new ArrayList<>();
        suppressCharacter.add(SINGLE_SPACE);
        suppressCharacter.add("");
        tokens.removeAll(suppressCharacter);
        return tokens;
    }

    private IEvaluable getOperand(ArrayList<String> tokens) {
        if (isRangeOp(tokens)) {
            return getRangeOp(tokens);
        }

        IEvaluable operand;
        if ("(".equals(tokens.get(0))) {
            tokens.remove(0); // Remove open brace
            int endIndex = this.findCloseBrace(0, tokens);
            ArrayList<String> brace = new ArrayList<>();
            tokens.remove(endIndex); // Remove close brace

            for (int index = 0; index < endIndex; index++) {
                brace.add(tokens.remove(0));
            }

            operand = this.parse("= " + String.join(" ", brace)); // Recursive
                                                                  // call
        } else {
            String operandToken = tokens.remove(0); // Removes cell or number
            operand = getSingleOperand(operandToken);
        }
        return operand;
    }

    /**
     * Returns an IEvaluable from a token. The token represents a Cell or a
     * number.
     */
    private IEvaluable getSingleOperand(String operandToken) {

        for (String pattern : parserHashMap.keySet()) {
            if (operandToken.matches(pattern)) {
                IParser parser = parserHashMap.get(pattern);
                return parser.parse(operandToken);
            }
        }

        return StringParser.getInstance().parse(operandToken); // Default is
                                                               // string
    }

    private void ifIsPrintfSetText(ArrayList<String> tokens) {

        if (!tokens.get(0).equals("PRINTF")) {
            return;
        }
        int idxPrimerComillas = tokens.get(2).indexOf('"');
        int idxSegundaComillas = tokens.get(2).lastIndexOf('"');

        String text = tokens.get(2).substring(idxPrimerComillas, idxSegundaComillas + 1);

        tokens.set(2, getCellsListToPrint(tokens.get(2), text));

        ((Printf) rangeOperations.get(tokens.get(0))).setText(text.replace("\\\\b", " "));

    }

    private String getCellsListToPrint(String token, String text) {
        return token.replace(text + ",", "");
    }
}
