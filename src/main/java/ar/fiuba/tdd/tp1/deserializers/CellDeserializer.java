package ar.fiuba.tdd.tp1.deserializers;

import com.google.gson.*;

import ar.fiuba.tdd.tp1.Cell;
import ar.fiuba.tdd.tp1.EvaluablesFactory;
import ar.fiuba.tdd.tp1.IEvaluable;
import ar.fiuba.tdd.tp1.formatters.DoubleFormatter;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.function.Function;

public class CellDeserializer implements JsonDeserializer<Cell> {

    private HashMap<String, Function<String, IEvaluable>> valueBuilders;

    public CellDeserializer() {
        super();
        valueBuilders = new HashMap<>();
        loadValueBuilders();
    }

    @SuppressWarnings("CPD-BEGIN")
    private void loadValueBuilders() {
        valueBuilders.put("Number", this::getNumberValue);
        valueBuilders.put("Date", this::getDateValue);
        valueBuilders.put("String", this::getStringValue);
        valueBuilders.put("Money", this::getMoneyValue);
    }

    @SuppressWarnings("CPD-END")
    public Cell deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonCell = json.getAsJsonObject();
        String id = jsonCell.get("id").getAsString();
        IEvaluable value = valueBuilders.get(jsonCell.get("type").getAsString()).apply(jsonCell.get("value").getAsString());
        Cell cell = new Cell(id, value);
        cell.setFormatter(context.deserialize(jsonCell.get("formatter"), DoubleFormatter.class));
        return cell;
    }

    private IEvaluable getNumberValue(String value) {
        return EvaluablesFactory.getNumber(Double.parseDouble(value));
    }

    private IEvaluable getDateValue(String value) {
        return EvaluablesFactory.getDate(value);
    }

    private IEvaluable getMoneyValue(String value) {
        String[] splitedValue = value.split(" ");
        return EvaluablesFactory.getMoney(splitedValue[0], Double.parseDouble(splitedValue[1]));
    }

    private IEvaluable getStringValue(String value) {
        return EvaluablesFactory.getString(value);
    }
}
