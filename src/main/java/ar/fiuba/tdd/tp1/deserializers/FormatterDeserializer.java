package ar.fiuba.tdd.tp1.deserializers;

import com.google.gson.*;

import ar.fiuba.tdd.tp1.formatters.*;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class FormatterDeserializer implements JsonDeserializer<DoubleFormatter> {

    @SuppressWarnings("rawtypes")
    private HashMap<String, Function<HashMap, DoubleFormatter>> formatterBuilders;

    public FormatterDeserializer() {
        super();
        formatterBuilders = new HashMap<>();
        loadFormattersBuilders();
    }

    private void loadFormattersBuilders() {
        formatterBuilders.put("Number", this::numberFormatterBuilder);
        formatterBuilders.put("Date", this::dateFormatterBuilder);
        formatterBuilders.put("Money", this::moneyFormatterBuilder);
    }

    public DoubleFormatter deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if (json.isJsonNull()) {
            return null;
        }
        String formatterType = null;
        HashMap<String, String> configs = new HashMap<>();
        for (Map.Entry<String, JsonElement> entry : json.getAsJsonObject().entrySet()) {
            String[] splitedKey = entry.getKey().split("\\.");
            formatterType = splitedKey[0];
            configs.put(splitedKey[1], entry.getValue().getAsString());
        }
        return formatterBuilders.get(formatterType).apply(configs);
    }

    private DoubleFormatter numberFormatterBuilder(HashMap<String, String> configs) {
        return FormatterFactory.getNumberFormatter(configs.get(NumberFormatter.DECIMAL));
    }

    private DoubleFormatter dateFormatterBuilder(HashMap<String, String> configs) {
        return FormatterFactory.getDateFormatter(configs.get(DateFormatter.FORMAT));
    }

    private DoubleFormatter moneyFormatterBuilder(HashMap<String, String> configs) {
        String symbol = configs.get(MoneyFormatter.SYMBOL);
        Boolean hasDecimals = Integer.parseInt(configs.get(MoneyFormatter.DECIMAL)) != 0;
        return FormatterFactory.getMoneyFormatter(symbol, hasDecimals);
    }
}
