package ar.fiuba.tdd.tp1.formatters;

import java.text.NumberFormat;

public class NumberFormatter extends DoubleFormatter {

    public static final String DECIMAL = "decimal";

    private static final int DEFAULT_DECIMALS = 2;

    public NumberFormatter() {
        this(DEFAULT_DECIMALS);
    }

    public NumberFormatter(int digits) {
        setParam(DECIMAL, Integer.toString(digits));
        dataType = "Number";
    }

    @Override
    public String formatDouble(double param) {
        NumberFormat nf = NumberFormat.getInstance();
        int decimals = Integer.parseInt(getParamValue(DECIMAL));
        if (decimals > 0) {
            nf.setMaximumFractionDigits(decimals);
            nf.setMinimumFractionDigits(decimals);
        }
        nf.setGroupingUsed(false);

        return nf.format(param).replace(".", ",");
    }

    @Override
    public String getErrorMsg() {
        return "Error:BAD_NUMBER";
    }
}
