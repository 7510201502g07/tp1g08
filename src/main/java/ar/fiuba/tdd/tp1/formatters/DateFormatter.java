package ar.fiuba.tdd.tp1.formatters;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.TimeZone;

public class DateFormatter extends DoubleFormatter {

    public static final String FORMAT = "format";

    public static final String DMY_FORMAT = "dd-MM-yyyy";
    public static final String MDY_FORMAT = "MM-dd-yyyy";
    public static final String YMD_FORMAT = "yyyy-MM-dd";

    public DateFormatter(String format) {
        setParam(FORMAT, format);
        dataType = "Date";
    }

    @Override
    public String formatDouble(double param) {
        long itemLong = (long) ((param) * 1000L);
        DateTime itemDate = new DateTime(itemLong, DateTimeZone.forTimeZone(TimeZone.getTimeZone("GTM+0")));

        String format = getParamValue(FORMAT);
        format = format.replace('D', 'd');
        format = format.replace('Y', 'y');
        DateTimeFormatter dtfOut = DateTimeFormat.forPattern(format);
        return dtfOut.print(itemDate);
    }

    @Override
    public String getErrorMsg() {
        return "Error:BAD_DATE";
    }
}
