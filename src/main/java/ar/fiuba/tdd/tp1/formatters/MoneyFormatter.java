package ar.fiuba.tdd.tp1.formatters;


import java.text.NumberFormat;

public class MoneyFormatter extends DoubleFormatter {

    public static final String SYMBOL = "symbol";
    public static final String DECIMAL = "decimal";

    public MoneyFormatter(String symbol, boolean hasDecimals) {
        setParam(SYMBOL, symbol);
        if (hasDecimals) {
            setParam(DECIMAL, "2");
        } else {
            setParam(DECIMAL, "0");
        }
        dataType = "Money";
    }

    @Override
    public String formatDouble(double param) {
        NumberFormat nf = NumberFormat.getInstance();
        nf.setGroupingUsed(false);
        try {
            int decimals = Integer.parseInt(getParamValue(DECIMAL));

            nf.setMaximumFractionDigits(decimals);
            nf.setMinimumFractionDigits(decimals);
            return getParamValue(SYMBOL) + " " + nf.format(param).replace(",", ".");
        } catch (Exception e) {
            return "Error:BAD_CURRENCY";
        }
    }

    @Override
    public String getErrorMsg() {
        return "Error:BAD_CURRENCY";
    }

}
