package ar.fiuba.tdd.tp1.formatters;

import java.util.HashMap;

public abstract class DoubleFormatter {

    protected String dataType;
    private HashMap<String, String> configs;

    public DoubleFormatter() {
        configs = new HashMap<>();
        dataType = null;
    }

    public abstract String formatDouble(double param);

    public void setParam(String param, String value) {
        configs.put(param, value);
    }

    protected String getParamValue(String param) {
        return configs.get(param);
    }

    public String getDataType() {
        return dataType;
    }

    public HashMap<String, String> getConfigs() {
        return configs;
    }

    public abstract String getErrorMsg();
}
