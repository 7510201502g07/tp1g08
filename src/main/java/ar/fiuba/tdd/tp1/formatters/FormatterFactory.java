package ar.fiuba.tdd.tp1.formatters;

public class FormatterFactory {

    public static DoubleFormatter getMoneyFormatter(String symbol) {
        return new MoneyFormatter(symbol, true);
    }

    public static DoubleFormatter getMoneyFormatter(String symbol, boolean decimals) {
        return new MoneyFormatter(symbol, decimals);
    }

    public static DoubleFormatter getDateYMDFormatter() {
        return new DateFormatter(DateFormatter.YMD_FORMAT);
    }

    public static DoubleFormatter getDateMDYFormatter() {
        return new DateFormatter(DateFormatter.MDY_FORMAT);
    }

    public static DoubleFormatter getDateDMYFormatter() {
        return new DateFormatter(DateFormatter.DMY_FORMAT);
    }

    public static DoubleFormatter getDateFormatter(String format) {
        return new DateFormatter(format);
    }

    public static DoubleFormatter getDateFormatter() {
        return new DateFormatter("");
    }

    public static DoubleFormatter getNumberFormatter() {
        return new NumberFormatter();
    }

    public static DoubleFormatter getNumberFormatter(int decimals) {
        return new NumberFormatter(decimals);
    }

    public static DoubleFormatter getNumberFormatter(String decimals) {
        return new NumberFormatter(Integer.parseInt(decimals));
    }

    public static DoubleFormatter getStringFormatter() {
        return new StringFormatter();
    }
}
