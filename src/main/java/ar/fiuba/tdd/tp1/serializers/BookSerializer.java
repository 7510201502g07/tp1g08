package ar.fiuba.tdd.tp1.serializers;

import com.google.gson.*;

import ar.fiuba.tdd.tp1.Book;
import ar.fiuba.tdd.tp1.Cell;
import ar.fiuba.tdd.tp1.Sheet;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class BookSerializer implements JsonSerializer<Book> {

    public JsonElement serialize(Book src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonBook = new JsonObject();
        jsonBook.addProperty("name", src.getName());
        JsonArray jsonCellsArray = new JsonArray();
        HashMap<String, Sheet> sheetMap = src.getSheetMap();
        for (Map.Entry<String, Sheet> sheetEntry : sheetMap.entrySet()) {
            HashMap<String, Cell> cellMap = sheetEntry.getValue().getCellMap();
            for (Map.Entry<String, Cell> cellEntry : cellMap.entrySet()) {
                JsonObject jsonCell = context.serialize(cellEntry.getValue()).getAsJsonObject();
                jsonCell.addProperty("sheet", sheetEntry.getKey());
                jsonCellsArray.add(jsonCell);
            }
        }
        jsonBook.add("cells", jsonCellsArray);
        return jsonBook;
    }
}
