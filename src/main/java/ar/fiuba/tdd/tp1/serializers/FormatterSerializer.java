package ar.fiuba.tdd.tp1.serializers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import ar.fiuba.tdd.tp1.formatters.DoubleFormatter;


import java.lang.reflect.Type;
import java.util.Map;

public class FormatterSerializer implements JsonSerializer<DoubleFormatter> {

    public JsonElement serialize(DoubleFormatter src, Type typeOfSrc, JsonSerializationContext context) {
        if (src.getDataType() == null) {
            return new JsonObject();
        }
        JsonObject jsonObject = new JsonObject();
        String type = src.getDataType();
        for (Map.Entry<String, String> entry : src.getConfigs().entrySet()) {
            jsonObject.addProperty(type + "." + entry.getKey(), entry.getValue());
        }
        return jsonObject;
    }
}
