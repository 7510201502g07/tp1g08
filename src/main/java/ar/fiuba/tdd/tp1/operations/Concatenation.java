package ar.fiuba.tdd.tp1.operations;

import ar.fiuba.tdd.tp1.IEvaluable;

import java.util.List;

public class Concatenation implements IRangeOperation {

    public String operate(List<IEvaluable> cellList) {
        StringBuilder result = new StringBuilder();
        for (IEvaluable c : cellList) {
            result.append(c.getValue());
        }
        return result.toString();
    }
    
}
