package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.formatters.DoubleFormatter;

/**
 * Cell of a sheet. Stores its ID and value.
 */
public class Cell implements IEvaluable {

    public IEvaluable value;
    private String id;
    private DoubleFormatter formatter;
    
    private boolean hasNamedRange = false;

    public Cell() {
        this(null);
    }

    public Cell(String id) {
        this.id = id;
        this.value = () -> {
            throw new IllegalArgumentException();
        };
        this.formatter = null;
    }

    public Cell(String id, IEvaluable value) {
        this.id = id;
        this.value = value;
    }

    public String getValue() {
        CircularReferenceChecker crChecker = CircularReferenceChecker.getInstance();
        crChecker.check(this.id);
        crChecker.add(this.id);
        if (formatter == null) {
            crChecker.removeLast();
            return this.value.getValue();
        }
        try {
            crChecker.removeLast();
            return formatter.formatDouble(value.asDouble());
        } catch (IllegalArgumentException e) {
            return formatter.getErrorMsg();
        }
    }

    public void setValue(IEvaluable value) {
        this.value = value;
    }

    public double asDouble() {
        CircularReferenceChecker crChecker = CircularReferenceChecker.getInstance();
        crChecker.check(this.id);
        crChecker.add(this.id);
        Double result = this.value.asDouble();
        crChecker.removeLast();
        return result;
    }

    @Override
    public DoubleFormatter getFormatter() {
        return formatter;
    }

    public void setFormatter(DoubleFormatter formatter) {
        this.formatter = formatter;
    }

    public void setFormatterParam(String param, String value) {
        this.formatter.setParam(param, value);
    }

    public String id() {
        return id;
    }

    public IEvaluable value() {
        return this.value;
    }
    
    public void hasNamedRange(boolean value) {
        this.hasNamedRange = value;
    }
    
    public boolean hasNamedRange() {
        return this.hasNamedRange;
    }    
}
