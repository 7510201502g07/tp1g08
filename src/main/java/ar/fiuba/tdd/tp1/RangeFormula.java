package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.operations.IRangeOperation;

import java.util.List;

public class RangeFormula implements IEvaluable {

    private final List<IEvaluable> cellList;
    private IRangeOperation operation;

    public RangeFormula(List<IEvaluable> cellList, IRangeOperation operation) {
        this.cellList = cellList;
        this.operation = operation;
    }

    public String getValue() {
        return asString();
    }

    public double asDouble() {
        return Double.parseDouble(this.operation.operate(cellList));
    }

    public String asString() {
        return String.valueOf(this.operation.operate(cellList));
    }
}
