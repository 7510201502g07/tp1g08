package ar.fiuba.tdd.tp1;

import com.google.gson.*;

import ar.fiuba.tdd.tp1.deserializers.BookDeserializer;
import ar.fiuba.tdd.tp1.deserializers.CellDeserializer;
import ar.fiuba.tdd.tp1.deserializers.FormatterDeserializer;
import ar.fiuba.tdd.tp1.formatters.DateFormatter;
import ar.fiuba.tdd.tp1.formatters.DoubleFormatter;
import ar.fiuba.tdd.tp1.formatters.MoneyFormatter;
import ar.fiuba.tdd.tp1.formatters.NumberFormatter;
import ar.fiuba.tdd.tp1.serializers.BookSerializer;
import ar.fiuba.tdd.tp1.serializers.CellSerializer;
import ar.fiuba.tdd.tp1.serializers.FormatterSerializer;

import java.io.*;

public class JsonPersistence {

    private Gson gson;
    private static JsonPersistence instance = null;

    private JsonPersistence() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        loadSerializers(gsonBuilder);
        loadDeserializers(gsonBuilder);
        gson = gsonBuilder.setPrettyPrinting().serializeNulls().create();
    }

    public static JsonPersistence getInstance() {
        if (instance == null) {
            instance = new JsonPersistence();
        }
        return instance;
    }

    public static void persistBook(Book book, String fileName) {
        try {
            Writer fileWriter = new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8");
            getInstance().gson.toJson(book, fileWriter);
            fileWriter.close();
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    public static Book loadBook(String fileName) {
        Book book;
        try {
            Reader fileReader = new InputStreamReader(new FileInputStream(fileName), "UTF-8");
            book = getInstance().gson.fromJson(fileReader, Book.class);
            fileReader.close();
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
        return book;
    }

    private void loadSerializers(GsonBuilder gsonBuilder) {
        gsonBuilder.registerTypeAdapter(Book.class, new BookSerializer());
        gsonBuilder.registerTypeAdapter(Cell.class, new CellSerializer());
        gsonBuilder.registerTypeAdapter(DoubleFormatter.class, new FormatterSerializer());
        gsonBuilder.registerTypeAdapter(NumberFormatter.class, new FormatterSerializer());
        gsonBuilder.registerTypeAdapter(MoneyFormatter.class, new FormatterSerializer());
        gsonBuilder.registerTypeAdapter(DateFormatter.class, new FormatterSerializer());
    }

    private void loadDeserializers(GsonBuilder gsonBuilder) {
        gsonBuilder.registerTypeAdapter(Book.class, new BookDeserializer());
        gsonBuilder.registerTypeAdapter(Cell.class, new CellDeserializer());
        gsonBuilder.registerTypeAdapter(DoubleFormatter.class, new FormatterDeserializer());
    }
}
