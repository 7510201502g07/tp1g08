package ar.fiuba.tdd.tp1;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CsvPersistence {

    private static char SEPARATOR = ';';
    private static int LETTERS_COUNT = 26;

    public static void writeSheet(Sheet sheet, String fileName) {
        int greaterColumnNumber = idToColumnNumber(getGreaterColumnId(sheet));
        int greaterRowNumber = getNumbersFromId(getGreaterRowId(sheet));
        List<String[]> valuesMatrix = new ArrayList<>(greaterRowNumber);
        for (int r = 1; r <= greaterRowNumber; r++) {
            List<String> row = new ArrayList<>(greaterColumnNumber);
            for (int c = 1; c <= greaterColumnNumber; c++) {
                row.add(getCellValue(sheet, c, r));
            }
            valuesMatrix.add(row.toArray(new String[row.size()]));
        }
        try {
            Writer fileWriter = new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8");
            CSVWriter csvWriter = new CSVWriter(fileWriter, SEPARATOR);
            csvWriter.writeAll(valuesMatrix);
            fileWriter.close();
            csvWriter.close();
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    public static List<Pair<String, String>> readSheet(String fileName) {
        List<Pair<String, String>> cellList = new LinkedList<>();
        try {
            Reader fileReader = new InputStreamReader(new FileInputStream(fileName), "UTF-8");
            CSVReader csvReader = new CSVReader(fileReader, SEPARATOR);
            int row = 1;
            String[] line;
            while ((line = csvReader.readNext()) != null) {
                for (int column = 1; column <= line.length; column++) {
                    String value = line[column - 1];
                    if (!value.equals("")) {
                        String cellId = getCellId(column, row);
                        cellList.add(new Pair<>(cellId, value));
                    }
                }
                row++;
            }
            fileReader.close();
            csvReader.close();
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
        return cellList;
    }

    private static String getGreaterColumnId(Sheet sheet) {
        String greaterColumnId = "";
        int greaterColumnIdValue = -1;
        for (Map.Entry<String, Cell> entry : sheet.getCellMap().entrySet()) {
            String id = entry.getKey();
            int columnIdValue = idToColumnNumber(id);
            if (columnIdValue > greaterColumnIdValue) {
                greaterColumnId = id;
                greaterColumnIdValue = columnIdValue;
            }
        }
        return greaterColumnId;
    }

    private static String getGreaterRowId(Sheet sheet) {
        String greaterRowId = "";
        int greaterRowIdValue = -1;
        for (Map.Entry<String, Cell> entry : sheet.getCellMap().entrySet()) {
            String id = entry.getKey();
            int rowIdValue = getNumbersFromId(id);
            if (rowIdValue > greaterRowIdValue) {
                greaterRowId = id;
                greaterRowIdValue = rowIdValue;
            }
        }
        return greaterRowId;
    }

    private static String getCellId(int column, int row) {
        return columnNumberToIdLetters(column) + String.valueOf(row);
    }

    private static String getCellValue(Sheet sheet, int column, int row) {
        String cellId = getCellId(column, row);
        return sheet.cellExists(cellId) ? sheet.getCell(cellId).getValue() : "";
    }

    private static int idToColumnNumber(String id) {
        int columnNumber = 0;
        String reversedId = new StringBuilder(getLettersFromId(id)).reverse().toString();
        for (int i = 0; i < reversedId.length(); i++) {
            int charValue = (int) (reversedId.charAt(i)) - (int) 'A' + 1;
            columnNumber += charValue * Math.pow(LETTERS_COUNT, i);
        }
        return columnNumber;
    }

    private static String columnNumberToIdLetters(int columnNumber) {
        StringBuilder idBuilder = new StringBuilder();
        while (columnNumber > 0) {
            int letterValue = (columnNumber % LETTERS_COUNT) + (int) 'A' - 1;
            idBuilder.append((char) letterValue);
            columnNumber /= LETTERS_COUNT;
        }
        return idBuilder.reverse().toString();
    }

    private static String getLettersFromId(String id) {
        Matcher matcher = Pattern.compile("([A-Z]+)[0-9]+").matcher(id);
        if (matcher.matches()) {
            return matcher.group(1);
        }
        return null;
    }

    private static int getNumbersFromId(String id) {
        Matcher matcher = Pattern.compile("[A-Z]+([0-9]+)").matcher(id);
        if (matcher.matches()) {
            return Integer.parseInt(matcher.group(1));
        }
        return -1;
    }
}
