package ar.fiuba.tdd.tp1;

import java.util.HashMap;

/**
 * A sheet that contains cells.
 */
public class Sheet {
    private HashMap<String, Cell> cellMap;

    public Sheet() {
        cellMap = new HashMap<>();
    }

    public final Cell getCell(String cellId) throws IllegalArgumentException {
        Cell optCell = cellMap.get(cellId);
        if (optCell == null) {
            this.addCell(cellId);
        }
        return cellMap.get(cellId);
    }

    public final Cell addCell(String cellId) {
        Cell cell = new Cell(cellId);
        cellMap.put(cellId, cell);
        return cell;
    }

    public final void addCell(String cellId, Cell cell) {
        cellMap.put(cellId, cell);
    }

    public final boolean cellExists(String cellId) {
        return cellMap.containsKey(cellId);
    }

    public final HashMap<String, Cell> getCellMap() {
        return cellMap;
    }
    
}

