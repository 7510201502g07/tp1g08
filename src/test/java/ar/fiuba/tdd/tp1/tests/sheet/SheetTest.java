package ar.fiuba.tdd.tp1.tests.sheet;

import ar.fiuba.tdd.tp1.Cell;
import ar.fiuba.tdd.tp1.Sheet;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SheetTest {

    private Sheet sheet;

    @Before
    public void setUp() {
        sheet = new Sheet();
        sheet.addCell("A1");
        sheet.addCell("A2", new Cell("A2"));
    }

    @Test
    public void getCellTest() {
        Cell cell = sheet.getCell("A1");
        assertTrue(cell != null);
        cell = sheet.getCell("A2");
        assertTrue(cell != null);
    }

    @Test
    public void cellExistsTest() {
        assertTrue(sheet.cellExists("A1"));
        assertFalse(sheet.cellExists("Nonexistent cell"));
    }
}
