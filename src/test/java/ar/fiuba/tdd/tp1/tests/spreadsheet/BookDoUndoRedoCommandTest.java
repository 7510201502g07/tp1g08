package ar.fiuba.tdd.tp1.tests.spreadsheet;

import ar.fiuba.tdd.tp1.Book;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class BookDoUndoRedoCommandTest {

    private Book ssheet;
    private ArrayList<String> parameters;

    @Before
    public void setUp() {
        ssheet = new Book();
        parameters = new ArrayList<>();
        parameters.add("H1");
        ssheet.createSheet(parameters);
        parameters.clear();
    }

    @After
    public void tearDown() {
        ssheet = null;
    }

    @Test
    public void simpleUndo2Test() {
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = -9".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 5".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        ssheet.undo();

        parameters.add("!H1.A1");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == -9);
    }

    @Test
    public void assignAndUndoTest() {
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = -9".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A2 = !H1.A1".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 5".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        ssheet.undo();

        parameters.add("!H1.A2");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == -9);
    }

    @Test
    public void assignAndUndoMultipleTest() {
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 6".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 7".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 8".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 9".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 10".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        ssheet.undo();
        ssheet.undo();
        ssheet.undo();
        ssheet.undo();

        parameters.add("!H1.A1");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == 6);
    }

    @Test
    public void assignOperateAndUndoTest() {
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = -9".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A2 = !H1.A1 + 1".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 5".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        ssheet.undo();

        parameters.add("!H1.A2");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == -8);
    }


    @Test
    public void assignAndRedo1Test() {
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = -9".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        ssheet.undo();
        ssheet.redo();

        parameters.add("!H1.A1");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == -9);
    }

    @Test
    public void assignAndRedo2Test() {
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = -9".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A2 = !H1.A1".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 5".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        ssheet.undo();
        ssheet.redo();

        parameters.add("!H1.A1");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == 5);
    }

    @Test
    public void assignOperateAndRedoTest() {
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = -9".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A2 = !H1.A1 - 1".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 5".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        ssheet.undo();
        ssheet.redo();

        parameters.add("!H1.A2");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == 4);
    }

    @Test
    public void assignAndUndoRedoMultipleTest() {
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 6".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 7".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 8".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 9".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 10".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        ssheet.undo();
        ssheet.undo();
        ssheet.undo();

        ssheet.redo();
        ssheet.redo();

        parameters.add("!H1.A1");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == 9);
    }

    @Test
    public void assignAndUndoRedoMultipleIntercalatedTest() {
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 7".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 8".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 9".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 10".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        ssheet.undo();
        ssheet.redo();

        ssheet.undo();
        ssheet.redo();

        ssheet.undo();
        ssheet.redo();

        ssheet.undo();

        parameters.add("!H1.A1");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == 9);
    }

    @Test(expected = IllegalArgumentException.class)
    public void undoToNoCellTest() {
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = -9".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        ssheet.undo();

        parameters.add("!H1.A1");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == -9);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nothingToUndoTest() {
        ssheet.undo();
        ssheet.undo(); //Fixme
    }

    @Test(expected = IllegalArgumentException.class)
    public void nothingToRedoTest() {
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = -9".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        ssheet.redo();
    }

    @Test(expected = IllegalArgumentException.class)
    public void assignUndoAssignAndRedoTest() {
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = -9".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A2 = !H1.A1 - 1".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 5".split(" ")));
        ssheet.assignValue(parameters);

        ssheet.undo();

        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 6".split(" ")));
        ssheet.assignValue(parameters);

        ssheet.redo();

        parameters.clear();

        parameters.add("!H1.A1");
        ssheet.getDoubleValue(parameters);
    }

}
