package ar.fiuba.tdd.tp1.tests.iteracion3era;

import ar.fiuba.tdd.tp1.Book;
import ar.fiuba.tdd.tp1.Cell;
import ar.fiuba.tdd.tp1.IEvaluable;
import ar.fiuba.tdd.tp1.Parser;
import ar.fiuba.tdd.tp1.RangeFormula;
import ar.fiuba.tdd.tp1.driver.SpreadSheetInterfaceAdapter;
import ar.fiuba.tdd.tp1.driver.SpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.iteracion3era.Printf;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class PrintfTest {

    Parser parser = Parser.getInstance();
    private List<IEvaluable> cellList;   
    
    private Printf printf;
    
    private Book ssheet;
    private ArrayList<String> parameters;
    
    private SpreadSheetTestDriver testDriver;
    
    @Before
    public void setUp() {
        cellList = new ArrayList<>();
        printf = new Printf();
        
        ssheet = new Book();
        parameters = new ArrayList<>();
        parameters.add("H1");
        ssheet.createSheet(parameters);
        parameters.clear();
        
        testDriver = new SpreadSheetInterfaceAdapter();
    }
    
    @Test
    public void printsWorksCorrectly() {       
        
        cellList.add(new Cell("C2", parser.parse("Juan")));
        cellList.add(new Cell("C3", parser.parse("Perez")));
        
        printf.setText("Hola $0 $1");
        
        RangeFormula formula = new RangeFormula(cellList, printf);
        
        String text = formula.getValue();
        
        assertEquals( "Hola Juan Perez", text);
        
    }
    
    @Test
    public void bookReturnValueCorrectly() {
        
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 Juan".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A2 Perez".split(" ")));
        ssheet.assignValue(parameters);
        
        parameters = new ArrayList<>(Arrays.asList("!H1.A3 = PRINTF(\"Hola\\b$0\\b$1\",!H1.A1,!H1.A2)".split(" ")));
        ssheet.assignValue(parameters);
        
        parameters.clear();
        
        parameters.add("!H1.A3");
        String text = ssheet.getValue(parameters);
        
        assertEquals( "\"Hola\\bJuan\\bPerez\"", text);
        
    }
    
    @Test
    public void returnErrorWhenBadFormulaCorrectly() {
        
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 Juan".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A2 Perez".split(" ")));
        ssheet.assignValue(parameters);
        
        parameters = new ArrayList<>(Arrays.asList("!H1.A3 = PRINTF(\"Hola\\b$0\\b$1\\b$2\",!H1.A1,!H1.A2)".split(" ")));
        ssheet.assignValue(parameters);
        
        parameters.clear();
        
        parameters.add("!H1.A3");
        String text = ssheet.getValue(parameters);
        
        assertEquals( "ERROR: BAD FORMULA", text);
        
    }
    
    @Test
    public void getCellValueAsStringWithPrintf() {
        
        testDriver.createNewWorkBookNamed("tecnicas");
        
        testDriver.setCellValue("tecnicas", "default", "A1", "Juan");
        testDriver.setCellValue("tecnicas", "default", "A2", "Perez");
        testDriver.setCellValue("tecnicas", "default", "A3", "Buenos Aires");
        testDriver.setCellValue("tecnicas", "default", "A4", "Argentina");
        testDriver.setCellValue("tecnicas", "default", "A5", "= PRINTF(\"Hola, soy $0 $1. Vivo en $2, $3.\",A1,A2,A3,A4)");
        assertEquals("\"Hola, soy Juan Perez. Vivo en Buenos Aires, Argentina.\"", testDriver.getCellValueAsString("tecnicas", "default", "A5"));
        
    }

}
