package ar.fiuba.tdd.tp1.tests.cell;

import ar.fiuba.tdd.tp1.Cell;
import ar.fiuba.tdd.tp1.Formula;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class CellTest {

    @Test
    public void makeInstanceTest() {
        new Cell("C1");
    }

    @Test
    public void nameAssignTest() {
        String cellName = "C3";
        Cell cell = new Cell(cellName);
        assertTrue(cell.id().equals(cellName));
    }

    @Test
    public void assignAndGetFormulaTest() {
        Cell cell = new Cell("A1");
        Formula formula = new Formula(new Cell("2"), new Cell("3"), (first, second) -> first + second);
        cell.setValue(formula);
        assertTrue(cell.value().equals(formula));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getEmptyCell() {
        Cell cell = new Cell("A1");
        cell.getValue();
    }
}
