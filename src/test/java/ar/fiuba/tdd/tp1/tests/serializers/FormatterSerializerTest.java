package ar.fiuba.tdd.tp1.tests.serializers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ar.fiuba.tdd.tp1.formatters.*;
import ar.fiuba.tdd.tp1.serializers.FormatterSerializer;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class FormatterSerializerTest {

    private Gson gson;

    @Before
    public void setUp() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(DoubleFormatter.class, new FormatterSerializer());
        gsonBuilder.registerTypeAdapter(NumberFormatter.class, new FormatterSerializer());
        gsonBuilder.registerTypeAdapter(MoneyFormatter.class, new FormatterSerializer());
        gsonBuilder.registerTypeAdapter(DateFormatter.class, new FormatterSerializer());
        gson = gsonBuilder.serializeNulls().create();
    }

    @Test
    public void numberFormatterSerializationTest() {
        DoubleFormatter numberFormatter = FormatterFactory.getNumberFormatter(4);
        String jsonResult = gson.toJson(numberFormatter);
        assertTrue(jsonResult.equals("{\"" + numberFormatter.getDataType() + "." + NumberFormatter.DECIMAL + "\":\"4\"}"));
    }

    @Test
    public void moneyFormatterSerializationTest() {
        DoubleFormatter moneyFormatter = FormatterFactory.getMoneyFormatter("$");
        String jsonResult = gson.toJson(moneyFormatter);
        assertTrue(jsonResult.equals("{\"" + moneyFormatter.getDataType() + "." + MoneyFormatter.SYMBOL + "\":\"$\",\""
                + moneyFormatter.getDataType() + "." + MoneyFormatter.DECIMAL + "\":\"2\"}"));
        moneyFormatter = FormatterFactory.getMoneyFormatter("$", false);
        jsonResult = gson.toJson(moneyFormatter);
        assertTrue(jsonResult.equals("{\"" + moneyFormatter.getDataType() + "." + MoneyFormatter.SYMBOL + "\":\"$\",\""
                + moneyFormatter.getDataType() + "." + MoneyFormatter.DECIMAL + "\":\"0\"}"));
    }

    @Test
    public void dateFormatterSerializationTest() {
        DoubleFormatter dateFormatter = FormatterFactory.getDateDMYFormatter();
        String jsonResult = gson.toJson(dateFormatter);
        assertTrue(jsonResult.equals("{\"" + dateFormatter.getDataType() + "." + DateFormatter.FORMAT + "\":\"dd-MM-yyyy\"}"));
    }
}
