package ar.fiuba.tdd.tp1.tests.formatter;

import ar.fiuba.tdd.tp1.formatters.DateFormatter;
import ar.fiuba.tdd.tp1.formatters.FormatterFactory;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class DateFormatterTest {

    @Test
    public void formatEpochPlusOneDMYTest() {
        String result = FormatterFactory.getDateFormatter(DateFormatter.DMY_FORMAT).formatDouble(86400);
        assertTrue(result.equals("02-01-1970"));
    }

    @Test
    public void formatEpochPlusOneMDYTest() {
        String result = FormatterFactory.getDateFormatter(DateFormatter.MDY_FORMAT).formatDouble(86400);
        assertTrue(result.equals("01-02-1970"));
    }

    @Test
    public void formatEpochPlusOneYMDTest() {
        String result = FormatterFactory.getDateFormatter(DateFormatter.YMD_FORMAT).formatDouble(86400);
        assertTrue(result.equals("1970-01-02"));
    }
}
