package ar.fiuba.tdd.tp1.tests.iteracion3era;

import ar.fiuba.tdd.tp1.driver.SpreadSheetInterfaceAdapter;
import ar.fiuba.tdd.tp1.driver.SpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.iteracion3era.NamedRanges;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class NamedRangesTest {

    private NamedRanges namedRanges = null;
    private SpreadSheetTestDriver testDriver;
    private static final double DELTA = 0.0001;
    
    @Before
    public void setUp() {
        namedRanges = new NamedRanges();
        namedRanges.addNamedRange( "RangoA", "A1:A10" );
        namedRanges.addNamedRange( "RangoB", "B1:B20" );
        namedRanges.addNamedRange( "MAX", "C1:C10" );
        
        testDriver = new SpreadSheetInterfaceAdapter();
    }
    
    @Test
    public void recognizeSimpleNamedRangesCorrectly() {
        
        String sinNamedRange = namedRanges.replaceNamedRangeIfItHas( "MIN(HOLAHOLA)" );
        String conNamedRange = namedRanges.replaceNamedRangeIfItHas( "AVG(HOLARangoAHOLAHOLA)");
        
        assertEquals( "MIN(HOLAHOLA)", sinNamedRange );
        assertEquals( "AVG(HOLAA1:A10HOLAHOLA)", conNamedRange);
        
    }

    @Test
    public void recognizeOnlyNamedRangesBetweenParenthesis() {
        
        namedRanges.addNamedRange( "MAX" , "C1:C10" );
        
        String sinRango = namedRanges.replaceNamedRangeIfItHas( "MAX(hola)" );
        String conRango = namedRanges.replaceNamedRangeIfItHas( "MAX(MAX)");
        
        assertEquals( "MAX(hola)", sinRango );
        assertEquals( "MAX(C1:C10)", conRango);
        
    }
    
    @Test
    public void recognizeSimpleRangesCorrectly() {
        
        String sinRange = namedRanges.replaceRangeIfItHas( "MIN(Z1:Z10)" );
        String conRange = namedRanges.replaceRangeIfItHas( "MIN(A1:A10)" );
        
        assertEquals( "MIN(Z1:Z10)", sinRange );
        assertEquals( "MIN(RangoA)", conRange );
    }
    
    @Test
    public void recognizeOnlyRangesBetweenParenthesis() {
        
        String sinRange = namedRanges.replaceRangeIfItHas( "MAX(Z1:Z10)" );
        String conRange = namedRanges.replaceRangeIfItHas( "MAX(C1:C10)" );
        
        assertEquals( "MAX(Z1:Z10)", sinRange );
        assertEquals( "MAX(MAX)", conRange );
    }
    
    @Test
    public void getCellValueAsDoubleWithNamedRange() {
        
        testDriver.createNewWorkBookNamed("tecnicas");
        
        testDriver.addNamedRangeToWorkbook("tecnicas", "RangoA", "A1:A4" );        
        
        testDriver.setCellValue("tecnicas", "default", "A1", "10");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "A3", "10");
        testDriver.setCellValue("tecnicas", "default", "A4", "20");
        testDriver.setCellValue("tecnicas", "default", "A5", "= MIN(RangoA)");
        assertEquals(10, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
        
    }
    
    @Test
    public void getCellValueAsStringWithNamedRange() {
        
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A5", "MIN(RangoA)");
        assertEquals("MIN(RangoA)", testDriver.getCellValueAsString("tecnicas", "default", "A5"));
        
    }
}
