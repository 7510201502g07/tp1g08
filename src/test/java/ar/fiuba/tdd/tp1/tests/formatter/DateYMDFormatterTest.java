package ar.fiuba.tdd.tp1.tests.formatter;


import ar.fiuba.tdd.tp1.formatters.FormatterFactory;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class DateYMDFormatterTest {
    @Test
    public void formatEpochTest() {

        String result = FormatterFactory.getDateYMDFormatter().formatDouble(0);
        assertTrue(result.equals("1970-01-01"));
    }

    @Test
    public void formatADayOneDecimalTest() {
        String result = FormatterFactory.getDateYMDFormatter().formatDouble(1445223683);
        assertTrue(result.equals("2015-10-19"));
    }

    @Test
    public void formatADayTwoDecimalTest() {
        String result = FormatterFactory.getDateYMDFormatter().formatDouble(1445223684);
        assertTrue(result.equals("2015-10-19"));
    }

    @Test
    public void formatADayThreeDecimalTest() {
        String result = FormatterFactory.getDateYMDFormatter().formatDouble(1445223685);
        assertTrue(result.equals("2015-10-19"));
    }
}
