package ar.fiuba.tdd.tp1.tests.formulas;

import ar.fiuba.tdd.tp1.Cell;
import ar.fiuba.tdd.tp1.Formula;
import ar.fiuba.tdd.tp1.Parser;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class FormulaTest {

    private Cell cell2;
    private Cell cell3;
    private Cell cellm4;
    private Cell cellC2;

    @Before
    public void setUp() {

        Parser parser = Parser.getInstance();

        cell2 = new Cell("2", parser.parse("2"));
        cell3 = new Cell("3", parser.parse("3"));
        cellm4 = new Cell("-4", parser.parse("-4"));
        cellC2 = new Cell("C2", cell2);
    }

    @Test
    public void numberNumberTest1() {
        Formula formula = new Formula(cell2, cell3, (firstOp, secondOp) -> firstOp + secondOp);
        assertTrue(Double.parseDouble(formula.getValue()) == 5);
    }

    @Test
    public void numberNumberTest2() {
        Formula formula = new Formula(cell3, cellm4, (firstOp, secondOp) -> firstOp + secondOp);
        assertTrue(Double.parseDouble(formula.getValue()) == -1);
    }

    @Test
    public void numberNumberTest3() {
        Formula formula = new Formula(cell2, cellm4, (firstOp, secondOp) -> firstOp - secondOp);
        assertTrue(Double.parseDouble(formula.getValue()) == 6);
    }

    @Test
    public void numberCellTest() {
        Formula formula = new Formula(cell2, cellC2, (firstOp, secondOp) -> firstOp + secondOp);
        assertTrue(Double.parseDouble(formula.getValue()) == 2 * Double.parseDouble(cell2.getValue()));
    }

    @Test
    public void numberFormulaTest() {
        Formula formulaSub = new Formula(cell2, cell3, (firstOp, secondOp) -> firstOp + secondOp);
        Formula formula = new Formula(formulaSub, cell3, (firstOp, secondOp) -> firstOp + secondOp);
        assertTrue(Double.parseDouble(formula.getValue()) == 8);
    }

    @Test
    public void cellFormulaTest() {
        Formula formulaSub = new Formula(cell3, cellC2, (firstOp, secondOp) -> firstOp - secondOp);
        Formula formula = new Formula(formulaSub, cell2, (firstOp, secondOp) -> firstOp + secondOp);
        assertTrue(Double.parseDouble(formula.getValue()) == 3);
    }

    @Test
    public void cellMultFormulaTest() {
        Formula formulaMul = new Formula(cell3, cellC2, (firstOp, secondOp) -> firstOp * secondOp);
        assertTrue(Double.parseDouble(formulaMul.getValue()) == 6);
    }
    
    @Test
    public void cellDivFormulaTest() {
        Formula formulaDiv = new Formula(cell3, cellC2, (firstOp, secondOp) -> firstOp / secondOp);
        assertTrue(Double.parseDouble(formulaDiv.getValue()) == 1.5);
    }
    
    @Test
    public void cellPowFormulaTest() {
        Formula formulaPow = new Formula(cell3, cell3,(firstOp, secondOp) -> Math.pow(firstOp,secondOp));
        assertTrue(Double.parseDouble(formulaPow.getValue()) == 27);
    }
    
    @Test
    public void formulaFormulaTest() {
        Formula formulaSub1 = new Formula(cell2, cell3, (firstOp, secondOp) -> firstOp + secondOp);
        Formula formulaSub2 = new Formula(cell2, cell3, (firstOp, secondOp) -> firstOp - secondOp);
        Formula formula = new Formula(formulaSub1, formulaSub2, (firstOp, secondOp) -> firstOp + secondOp);
        assertTrue(Double.parseDouble(formula.getValue()) == 4);
    }
}
