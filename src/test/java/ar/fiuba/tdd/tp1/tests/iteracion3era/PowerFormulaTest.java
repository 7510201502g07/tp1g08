package ar.fiuba.tdd.tp1.tests.iteracion3era;

import ar.fiuba.tdd.tp1.driver.SpreadSheetInterfaceAdapter;
import ar.fiuba.tdd.tp1.driver.SpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.exceptions.BadFormulaException;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PowerFormulaTest {

    private SpreadSheetTestDriver testDriver;
    private static final double DELTA = 0.0001;
    
    @Before
    public void setUp() {
        testDriver = new SpreadSheetInterfaceAdapter();
    }
    
    @Test
    public void powerFormulaWithOnlyNumbers() {
        
        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver.setCellValue("tecnicas", "default", "A1", "= 3 ^ 3");
        assertEquals(27, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
        testDriver.setCellValue("tecnicas", "default", "A2", "= 3 ^ 2");
        assertEquals(9, testDriver.getCellValueAsDouble("tecnicas", "default", "A2"), DELTA);
        testDriver.setCellValue("tecnicas", "default", "A3", "= 2 ^ 3");
        assertEquals(8, testDriver.getCellValueAsDouble("tecnicas", "default", "A3"), DELTA);
    }
    
    @Test
    public void powerFormulaWithNumberAndCell() {
        
        testDriver.createNewWorkBookNamed("tecnicas");
        
        testDriver.setCellValue("tecnicas", "default", "A1", "3");
        testDriver.setCellValue("tecnicas", "default", "A2", "= A1 ^ 3");
        assertEquals(27, testDriver.getCellValueAsDouble("tecnicas", "default", "A2"), DELTA);
        testDriver.setCellValue("tecnicas", "default", "A3", "= 3 ^ A1");
        assertEquals(27, testDriver.getCellValueAsDouble("tecnicas", "default", "A3"), DELTA);
    }
    
    @Test
    public void powerFormulaWithOnlyCells() {
        
        testDriver.createNewWorkBookNamed("tecnicas");
        
        testDriver.setCellValue("tecnicas", "default", "A1", "3");
        testDriver.setCellValue("tecnicas", "default", "A2", "3");
        testDriver.setCellValue("tecnicas", "default", "A3", "= A1 ^ A2");
        assertEquals(27, testDriver.getCellValueAsDouble("tecnicas", "default", "A3"), DELTA);
        testDriver.setCellValue("tecnicas", "default", "A4", "= A2 ^ A1");
        assertEquals(27, testDriver.getCellValueAsDouble("tecnicas", "default", "A4"), DELTA);
        testDriver.setCellValue("tecnicas", "default", "A5", "= A2 ^ A2");
        assertEquals(27, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
    }
    
    @Test(expected = BadFormulaException.class)
    public void powerFormulaWithNumberAndString() {
        
        testDriver.createNewWorkBookNamed("tecnicas");
        
        testDriver.setCellValue("tecnicas", "default", "A1", "3");
        testDriver.setCellValue("tecnicas", "default", "A2", "hola");
        testDriver.setCellValue("tecnicas", "default", "A3", "= A1 ^ A2");
        assertEquals(27, testDriver.getCellValueAsDouble("tecnicas", "default", "A3"), DELTA);
        testDriver.setCellValue("tecnicas", "default", "A4", "= elevado ^ A1");
        assertEquals(27, testDriver.getCellValueAsDouble("tecnicas", "default", "A4"), DELTA);
        testDriver.setCellValue("tecnicas", "default", "A5", "= 3 ^ alCuadrado");
        assertEquals(27, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
    }
    
    @Test(expected = BadFormulaException.class)
    public void powerFormulaWithOnlyStrings() {
        
        testDriver.createNewWorkBookNamed("tecnicas");
        
        testDriver.setCellValue("tecnicas", "default", "A1", "tres");
        testDriver.setCellValue("tecnicas", "default", "A2", "alCuadrado");
        testDriver.setCellValue("tecnicas", "default", "A3", "= A1 ^ A2");
        assertEquals(27, testDriver.getCellValueAsDouble("tecnicas", "default", "A3"), DELTA);
        testDriver.setCellValue("tecnicas", "default", "A4", "= dos ^ alCubo");
        assertEquals(27, testDriver.getCellValueAsDouble("tecnicas", "default", "A4"), DELTA);
        testDriver.setCellValue("tecnicas", "default", "A5", "= A1 ^ alCuadrado");
        assertEquals(27, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
    }

}
