package ar.fiuba.tdd.tp1.tests.iteracion3era;

import ar.fiuba.tdd.tp1.driver.SpreadSheetInterfaceAdapter;
import ar.fiuba.tdd.tp1.driver.SpreadSheetTestDriver;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class LeftAndRightFormulaTest {

    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        testDriver = new SpreadSheetInterfaceAdapter();
    }

    @Test
    public void opLeftTest() {

        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver.setCellValue("tecnicas", "default", "A1", "= LEFT(holamundo,4)");
        assertTrue("hola".equals(testDriver.getCellValueAsString("tecnicas", "default", "A1")));
    }

    @Test
    public void opLeftZeroTest() {

        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver.setCellValue("tecnicas", "default", "A1", "= LEFT(holamundo,0)");
        assertTrue("".equals(testDriver.getCellValueAsString("tecnicas", "default", "A1")));
    }

    @Test
    public void opLeftOverflowTest() {

        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver.setCellValue("tecnicas", "default", "A1", "= LEFT(holamundo,14)");
        assertTrue("holamundo".equals(testDriver.getCellValueAsString("tecnicas", "default", "A1")));
    }

    @Test
    public void opRightTest() {

        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver.setCellValue("tecnicas", "default", "A1", "= RIGHT(holamundo,5)");
        assertTrue("mundo".equals(testDriver.getCellValueAsString("tecnicas", "default", "A1")));
    }

    @Test
    public void opRightZeroTest() {

        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver.setCellValue("tecnicas", "default", "A1", "= RIGHT(holamundo,0)");
        assertTrue("".equals(testDriver.getCellValueAsString("tecnicas", "default", "A1")));
    }

    @Test
    public void opRightOverflowTest() {

        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver.setCellValue("tecnicas", "default", "A1", "= RIGHT(holamundo,14)");
        assertTrue("holamundo".equals(testDriver.getCellValueAsString("tecnicas", "default", "A1")));
    }

}
