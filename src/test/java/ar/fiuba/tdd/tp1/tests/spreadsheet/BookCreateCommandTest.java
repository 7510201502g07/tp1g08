package ar.fiuba.tdd.tp1.tests.spreadsheet;

import ar.fiuba.tdd.tp1.Book;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertTrue;

public class BookCreateCommandTest {

    private Book ssheet;
    private ArrayList<String> parameters;

    @Before
    public void setUp() {
        ssheet = new Book();
        parameters = new ArrayList<>();
    }

    @Test
    public void createSheetTest() {
        parameters.add("createSheetTest");
        String result = ssheet.createSheet(parameters);
        assertTrue(result.equals("OK"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void createSheetThatExistsTest() {
        parameters.add("createSheetThatExists");
        ssheet.createSheet(parameters);
        ssheet.createSheet(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createWithNoIdTest() {
        ssheet.createSheet(parameters);
    }
}
