package ar.fiuba.tdd.tp1.tests.iteracion3era;

import ar.fiuba.tdd.tp1.driver.SpreadSheetInterfaceAdapter;
import ar.fiuba.tdd.tp1.driver.SpreadSheetTestDriver;

import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertTrue;

public class TodayFormulaTest {

    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        testDriver = new SpreadSheetInterfaceAdapter();
    }

    @Test
    public void test() {

        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver.setCellValue("tecnicas", "default", "A1", "= TODAY()");

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();

        assertTrue(dateFormat.format(date).equals(testDriver.getCellValueAsString("tecnicas", "default", "A1")));
    }

}
