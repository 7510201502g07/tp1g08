package ar.fiuba.tdd.tp1.tests.formulas;

import ar.fiuba.tdd.tp1.*;
import ar.fiuba.tdd.tp1.operations.Average;
import ar.fiuba.tdd.tp1.operations.Concatenation;
import ar.fiuba.tdd.tp1.operations.Maximum;
import ar.fiuba.tdd.tp1.operations.Minimum;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class RangeFormulaTest {

    Parser parser = Parser.getInstance();
    private List<IEvaluable> cellList1;
    private List<IEvaluable> cellList2;
    private List<IEvaluable> cellList3;
    private Average avg = new Average();
    private Maximum max = new Maximum();
    private Minimum min = new Minimum();
    private Concatenation concat = new Concatenation();

    @Before
    public void setUp() {

        cellList1 = new ArrayList<>();


        cellList1.add(new Cell("C2", parser.parse("2")));
        cellList1.add(new Cell("C3", parser.parse("3")));
        cellList1.add(new Cell("C4", parser.parse("-4")));

        cellList2 = new ArrayList<>();
        cellList2.add(new Cell("A1", parser.parse("HOLA")));
        cellList2.add(new Cell("A2"));
        cellList2.add(new Cell("A3"));

        cellList3 = new ArrayList<>();
        cellList3.add(new Cell("D1", parser.parse("HOLA")));
        cellList3.add(new Cell("D2", parser.parse("TDD")));
    }

    @After
    public void clearCache() {
        CircularReferenceChecker crChecker = CircularReferenceChecker.getInstance();
        crChecker.clear();
    }

    @Test
    public void avgOnlyNumbersTest() {
        RangeFormula formula = new RangeFormula(cellList1, avg);

        assertTrue(formula.asDouble() == 1.0 / 3);
    }

    @Test
    public void avgNumbersAndStringsTest() {
        cellList1.addAll(cellList2);
        RangeFormula formula = new RangeFormula(cellList1, avg);

        assertTrue(formula.asDouble() == 1.0 / 3);
    }

    @Test
    public void avgOnlyInvalidValuesTest() {
        RangeFormula formula = new RangeFormula(cellList2, avg);

        assertTrue(formula.asDouble() == 0);
    }

    @Test
    public void maxOnlyNumbersTest() {
        RangeFormula formula = new RangeFormula(cellList1, max);

        assertTrue(formula.asDouble() == 3);
    }

    @Test
    public void maxNumbersAndStringsTest() {
        cellList1.addAll(cellList2);
        RangeFormula formula = new RangeFormula(cellList1, max);

        assertTrue(formula.asDouble() == 3);
    }

    @Test
    public void maxOnlyInvalidValuesTest() {
        RangeFormula formula = new RangeFormula(cellList2, avg);

        assertTrue(formula.asDouble() == 0);
    }

    @Test
    public void minOnlyNumbersTest() {
        RangeFormula formula = new RangeFormula(cellList1, min);

        assertTrue(formula.asDouble() == -4);
    }

    @Test
    public void minNumbersAndStringsTest() {
        cellList1.addAll(cellList2);
        RangeFormula formula = new RangeFormula(cellList1, min);

        assertTrue(formula.asDouble() == -4);
    }

    @Test
    public void minOnlyInvalidValuesTest() {
        RangeFormula formula = new RangeFormula(cellList2, avg);

        assertTrue(formula.asDouble() == 0);
    }

    @Test
    public void concatStringCellsTest() {
        RangeFormula formula = new RangeFormula(cellList3, concat);

        assertTrue(formula.asString().equals("HOLATDD"));
    }

    @Test
    public void concatStringAndFormulaCellsTest() {
        Cell cell2 = new Cell("C2", parser.parse("2"));
        Cell cell3 = new Cell("C3", cell2);
        Formula binaryFormula = new Formula(cell2, cell3, (firstOp, secondOp) -> firstOp + secondOp);
        Cell cell4 = new Cell("C4", binaryFormula);

        List<IEvaluable> cellList = new ArrayList<>();
        cellList.add(cell2);
        cellList.add(cell3);
        cellList.add(cell4);

        RangeFormula rangeFormula = new RangeFormula(cellList, concat);

        assertTrue(rangeFormula.asString().equals("224.0"));
    }

//    @Ignore
//    @Test
//    public void concatStringAndDateCellsTest() {
//        return;
//    }


}