package ar.fiuba.tdd.tp1.tests.pair;

import ar.fiuba.tdd.tp1.Pair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class PairTests {

    private Pair<Integer, Integer> pair;

    @Before
    public void setUp() {
        pair = new Pair<>(2, 3);
    }

    @After
    public void tearDown() {
        pair = null;
    }

    @Test
    public void convertToStringTest() {
        String result = pair.toString();
        assertTrue(result.equals("(2 , 3)"));
    }

    @Test
    public void getFirst() {
        assertTrue(pair.first() == 2);
    }

    @Test
    public void getSecond() {
        assertTrue(pair.second() == 3);
    }

    @Test
    public void setFirst() {
        pair.first(9);
        assertTrue(pair.first() == 9);
    }

    @Test
    public void setSecond() {
        pair.second(-19);
        assertTrue(pair.second() == -19);
    }

    @Test
    public void setDontConflict1() {
        pair.first(9);
        assertTrue(pair.second() == 3);
    }

    @Test
    public void setDontConflict2() {
        pair.second(-19);
        assertTrue(pair.first() == 2);
    }

}
