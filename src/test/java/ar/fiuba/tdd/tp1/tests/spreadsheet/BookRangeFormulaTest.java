package ar.fiuba.tdd.tp1.tests.spreadsheet;

import ar.fiuba.tdd.tp1.Book;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class BookRangeFormulaTest {

    private Book ssheet;
    private ArrayList<String> parameters;

    @Before
    public void createSheet() {
        ssheet = new Book();
        parameters = new ArrayList<>();
        parameters.add("H1");
        ssheet.createSheet(parameters);
        parameters.clear();
        parameters.add("H2");
        ssheet.createSheet(parameters);
        parameters.clear();
    }

    @After
    public void tearDown() {
        ssheet = null;
        parameters = null;
    }

    @Test
    public void concatStringsTest() {
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 Hola".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A2 TDD".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A3 = CONCAT(!H1.A1,!H1.A2)".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        parameters.add("!H1.A3");
        String result = ssheet.getValue(parameters);
        assertEquals(result, "HolaTDD");
    }

}
