package ar.fiuba.tdd.tp1.tests.spreadsheet;

import ar.fiuba.tdd.tp1.Book;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BookDoComplexCommandTest {

    private Book ssheet;
    private ArrayList<String> parameters;

    @Before
    public void setUp() {
        ssheet = new Book();
        parameters = new ArrayList<>();
        parameters.add("H1");
        ssheet.createSheet(parameters);
        parameters.clear();
    }

    @After
    public void tearDown() {
        ssheet = null;
    }

    @Test
    public void assignMultipleNumbersSumAndGetTest() {
        parameters = new ArrayList<>(Arrays.asList("!H1.C1 = 2 + 2 + 1 + 1 + 3".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        parameters.add("!H1.C1");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == 2 + 2 + 1 + 1 + 3);
    }

    @Test
    public void assignMultipleNumbersSubtractAndGetTest() {
        parameters = new ArrayList<>(Arrays.asList("!H1.C1 = 2 - 2 - 1 - 1 - 3".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        parameters.add("!H1.C1");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == 0 - 1 - 1 - 3);
    }

    @Test
    public void assignMultipleNumbersSumAndSubtractAndGetTest() {
        parameters = new ArrayList<>(Arrays.asList("!H1.C1 = 2 - 2 + 5 + 7 + 1 - 1 - 3".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        parameters.add("!H1.C1");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == (5 + 7 + 1) - 1 - 3);
    }

    @Test
    public void assignMultipleNumbersAndCellAndGetTest() {
        parameters = new ArrayList<>(Arrays.asList("!H1.C2 = 2".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.C1 = 2 + 2 + 1 + 3 + !H1.C2".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        parameters.add("!H1.C1");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == 2 + 2 + 1 + 3 + 2);
    }

    @Test
    public void assignMultipleNumbersAndCellsAndGet1Test() {
        parameters = new ArrayList<>(Arrays.asList("!H1.C2 = 2".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.C3 = -1".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.C4 = 9.9".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.C1 = 2 + 2 + 1 + 3 + !H1.C2 + !H1.C3 + !H1.C4".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        parameters.add("!H1.C1");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == 2 + 2 + 1 + 3 + 2 - 1 + 9.9);
    }

    @Test
    public void assignMultipleNumbersAndCellsAndGet2Test() {
        parameters = new ArrayList<>(Arrays.asList("!H1.C2 = 5".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.C3 = -13".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.C4 = 9.9".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.C5 = 0.13".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.C1 = 2 - 2 + 1 + 3 + !H1.C2 + !H1.C5 - !H1.C3 + !H1.C4".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        parameters.add("!H1.C1");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == 32.03);
    }

    @Test
    public void assignMultipleNumbersAndCellsAndGet3Test() {
        parameters = new ArrayList<>(Arrays.asList("!H1.C1 = 1".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.C2 = 2".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.C3 = !H1.C1 + !H1.C1 - ( !H1.C2 + !H1.C1 + !H1.C2 )".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        parameters.add("!H1.C3");
        double result = ssheet.getDoubleValue(parameters);
        assertEquals(result, -3, 0.01);
    }

    @Test
    public void assignMultipleNumbersAndCellsAndGet4Test() {
        parameters = new ArrayList<>(Arrays.asList("!H1.C1 = 1".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.C2 = !H1.C1 + 2".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.C3 = !H1.C1 + !H1.C1 - ( !H1.C2 + !H1.C1 + !H1.C2 )".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.C4 = !H1.C3 - ( !H1.C1 - !H1.C3 ) + !H1.C2 - !H1.C1".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        parameters.add("!H1.C4");
        double result = ssheet.getDoubleValue(parameters);
        double partial1 = 1 + 2;
        double partial2 = 1 + 1 - (partial1 + 1 + partial1);
        double partial3 = partial2 - (1 - partial2) + partial1 - 1;
        assertTrue(result == partial3);
    }
}